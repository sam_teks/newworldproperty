var BGImg = {
    //main page background
    "b1": "main.png"
}

var btnImg = {
    //fifteen btns on the second page --- under the b1b1 button.
    "b1d1": "tabletennis.png",
    "b1d2": "billard.png",
    "b1d3": "swimmingpool.png",
    "b1d4": "gym.png",
    "b1d5": "bathroom.png",
    "b1d6": "sauna.png",
    "b1d7": "steam.png",
    "b1d8": "bowling.png",
    "b1d9": "bbq.png",
    "b1d10": "piano.png",
    "b1d11": "yoga.png",
    "b1d12": "ballroom1.png",
    "b1d13": "ballroom2.png",
    "b1d14": "ballroom3.png",
    "b1d15": "ballroom5.png",
}

var detailImg = {
    //fifteen btns on the second page --- under the b1b1 button.
    "1": "tabletennis.jpg",
    "2": "billard.jpg",
    "3": "swimmingpool.jpg",
    "4": "gym.jpg",
    "5": "bathroom.jpg",
    "6": "sauna.jpg",
    "7": "steam.jpg",
    "8": "bowling.jpg",
    "9": "bbq.jpg",
    "10": "piano.jpg",
    "11": "yoga.jpg",
    "12": "ballroom1.jpg",
    "13": "ballroom2.jpg",
    "14": "ballroom3.jpg",
    "15": "ballroom5.jpg",
}
