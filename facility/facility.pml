<?xml version="1.0" encoding="UTF-8" ?>
<Package name="facility" format_version="4">
    <Manifest src="manifest.xml" />
    <BehaviorDescriptions>
        <BehaviorDescription name="behavior" src="main" xar="behavior.xar" />
    </BehaviorDescriptions>
    <Dialogs />
    <Resources>
        <File name="bootstrap.min" src="html/css/bootstrap.min.css" />
        <File name="btnconfig" src="html/css/btnconfig.css" />
        <File name="common" src="html/css/common.css" />
        <File name="style" src="html/css/style.css" />
        <File name="ballroom1" src="html/img/ballroom1.png" />
        <File name="ballroom2" src="html/img/ballroom2.png" />
        <File name="ballroom3" src="html/img/ballroom3.png" />
        <File name="ballroom5" src="html/img/ballroom5.png" />
        <File name="bathroom" src="html/img/bathroom.png" />
        <File name="bowling" src="html/img/bowling.png" />
        <File name="main" src="html/img/main.png" />
        <File name="piano" src="html/img/piano.png" />
        <File name="qrcode" src="html/img/qrcode.jpg" />
        <File name="recording" src="html/img/recording.gif" />
        <File name="yoga" src="html/img/yoga.png" />
        <File name="index" src="html/index.html" />
        <File name="angular-PapaParse.min" src="html/js/angular-PapaParse.min.js" />
        <File name="angular-route.min" src="html/js/angular-route.min.js" />
        <File name="angular-touch.min" src="html/js/angular-touch.min.js" />
        <File name="angular.min" src="html/js/angular.min.js" />
        <File name="bootstrap.min" src="html/js/bootstrap.min.js" />
        <File name="imgConfig" src="html/js/imgConfig.js" />
        <File name="jquery-1.11.0.min" src="html/js/jquery-1.11.0.min.js" />
        <File name="jquery-3.1.1.min" src="html/js/jquery-3.1.1.min.js" />
        <File name="js.cookie" src="html/js/js.cookie.js" />
        <File name="main" src="html/js/main.js" />
        <File name="moment.min" src="html/js/moment.min.js" />
        <File name="robotutils" src="html/js/robotutils.js" />
        <File name="robotutils_2.0" src="html/js/robotutils_2.0.js" />
        <File name="font-awesome.min" src="html/css/font-awesome.min.css" />
        <File name="DINRoundOffcPro-Black" src="html/fonts/DINRoundOffcPro-Black.ttf" />
        <File name="DINRoundOffcPro-Bold" src="html/fonts/DINRoundOffcPro-Bold.ttf" />
        <File name="DINRoundOffcPro-Light" src="html/fonts/DINRoundOffcPro-Light.ttf" />
        <File name="DINRoundOffcPro-Medium" src="html/fonts/DINRoundOffcPro-Medium.ttf" />
        <File name="DINRoundOffcPro" src="html/fonts/DINRoundOffcPro.ttf" />
        <File name="FontAwesome" src="html/fonts/FontAwesome.otf" />
        <File name="fontawesome-webfont" src="html/fonts/fontawesome-webfont.eot" />
        <File name="fontawesome-webfont" src="html/fonts/fontawesome-webfont.svg" />
        <File name="fontawesome-webfont" src="html/fonts/fontawesome-webfont.ttf" />
        <File name="fontawesome-webfont" src="html/fonts/fontawesome-webfont.woff" />
        <File name="fontawesome-webfont" src="html/fonts/fontawesome-webfont.woff2" />
        <File name="transparent" src="html/img/transparent.png" />
        <File name="btnconfig" src="html/css/.sass-cache/5c537d63f47b2ae7ebdaa3364da806ae30d76166/btnconfig.scssc" />
        <File name="btnconfig.css" src="html/css/btnconfig.css.map" />
        <File name="btnconfig" src="html/css/btnconfig.scss" />
        <File name="btnconfig0" src="html/css/btnconfig0.css" />
        <File name="slideup" src="html/img/slideup.jpg" />
        <File name="index" src="html/app/js/index.js" />
        <File name="bbq" src="html/img/bbq.png" />
        <File name="billard" src="html/img/billard.png" />
        <File name="facility" src="html/img/facility.png" />
        <File name="facility_bak" src="html/img/facility_bak.png" />
        <File name="gym" src="html/img/gym.png" />
        <File name="sauna" src="html/img/sauna.png" />
        <File name="steam" src="html/img/steam.png" />
        <File name="swimmingpool" src="html/img/swimmingpool.png" />
        <File name="test1" src="html/img/test1.jpg" />
        <File name="ballroom1" src="html/img/details/ballroom1.jpg" />
        <File name="ballroom2" src="html/img/details/ballroom2.jpg" />
        <File name="ballroom5" src="html/img/details/ballroom5.jpg" />
        <File name="bathroom" src="html/img/details/bathroom.jpg" />
        <File name="bbq" src="html/img/details/bbq.jpg" />
        <File name="billard" src="html/img/details/billard.jpg" />
        <File name="bowling" src="html/img/details/bowling.jpg" />
        <File name="gym" src="html/img/details/gym.jpg" />
        <File name="piano" src="html/img/details/piano.jpg" />
        <File name="sauna" src="html/img/details/sauna.jpg" />
        <File name="steam" src="html/img/details/steam.jpg" />
        <File name="swimmingpool" src="html/img/details/swimmingpool.jpg" />
        <File name="tabletennis" src="html/img/details/tabletennis.jpg" />
        <File name="yoga" src="html/img/details/yoga.jpg" />
    </Resources>
    <Topics />
    <IgnoredPaths />
    <Translations auto-fill="en_US">
        <Translation name="translation_en_US" src="translations/translation_en_US.ts" language="en_US" />
        <Translation name="translation_zh_CN" src="translations/translation_zh_CN.ts" language="zh_CN" />
        <Translation name="translation_zh_TW" src="translations/translation_zh_TW.ts" language="zh_TW" />
    </Translations>
</Package>
