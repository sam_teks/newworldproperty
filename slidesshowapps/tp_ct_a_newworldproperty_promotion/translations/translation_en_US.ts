<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US">
    <context>
        <name>behavior.xar:/Get Localized Text</name>
        <message>
            <source>Hello</source>
            <comment>Text</comment>
            <translation type="vanished">Hello</translation>
        </message>
        <message>
            <source>test</source>
            <comment>Text</comment>
            <translation type="obsolete">test</translation>
        </message>
        <message>
            <source>facility</source>
            <comment>Text</comment>
            <translation type="obsolete">facility</translation>
        </message>
    </context>
    <context>
        <name>main/behavior.xar:/Get Localized Text</name>
        <message>
            <location filename="main/behavior.xar" line="0"/>
            <source>facility</source>
            <comment>Text</comment>
            <translation type="unfinished">facility</translation>
        </message>
    </context>
</TS>
