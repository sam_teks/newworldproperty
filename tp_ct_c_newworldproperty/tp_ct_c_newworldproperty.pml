<?xml version="1.0" encoding="UTF-8" ?>
<Package name="tp_ct_c_newworldproperty" format_version="4">
    <Manifest src="manifest.xml" />
    <BehaviorDescriptions>
        <BehaviorDescription name="behavior" src="behavior_1" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="Intllvdance" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="datedance" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="hungdance" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="main" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="photo_time_ct" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="photo_time_en" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="pose_handshake" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="pose_high5" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="pose_hug" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="test" xar="behavior.xar" />
    </BehaviorDescriptions>
    <Dialogs />
    <Resources>
        <File name="" src=".DS_Store" />
        <File name="InternationalLove" src="Intllvdance/InternationalLove.mp3" />
        <File name="music_all" src="datedance/music_all.mp3" />
        <File name="music_all_ori" src="datedance/music_all_ori.mp3" />
        <File name="" src="html/.DS_Store" />
        <File name="01_applause" src="html/01_applause.ogg" />
        <File name="FutuMd" src="html/app/css/FutuMd.ttf" />
        <File name="MSblack" src="html/app/css/MSblack.ttf" />
        <File name="btnconfig" src="html/app/css/btnconfig.css" />
        <File name="common" src="html/app/css/common.css" />
        <File name="style" src="html/app/css/style.css" />
        <File name="style_plus" src="html/app/css/style_plus.css" />
        <File name="action" src="html/app/js/action.js" />
        <File name="click" src="html/click.ogg" />
        <File name="click" src="html/click.wav" />
        <File name="bootstrap.min" src="html/css/bootstrap.min.css" />
        <File name="font-awesome.min" src="html/css/font-awesome.min.css" />
        <File name="plyr" src="html/css/plyr.css" />
        <File name="quiz" src="html/data/quiz.py" />
        <File name="quizQuestion" src="html/data/quizQuestion.json" />
        <File name="DINRoundOffcPro-Black" src="html/fonts/DINRoundOffcPro-Black.ttf" />
        <File name="DINRoundOffcPro-Bold" src="html/fonts/DINRoundOffcPro-Bold.ttf" />
        <File name="DINRoundOffcPro-Light" src="html/fonts/DINRoundOffcPro-Light.ttf" />
        <File name="DINRoundOffcPro-Medium" src="html/fonts/DINRoundOffcPro-Medium.ttf" />
        <File name="DINRoundOffcPro" src="html/fonts/DINRoundOffcPro.ttf" />
        <File name="FontAwesome" src="html/fonts/FontAwesome.otf" />
        <File name="fontawesome-webfont" src="html/fonts/fontawesome-webfont.eot" />
        <File name="fontawesome-webfont" src="html/fonts/fontawesome-webfont.svg" />
        <File name="fontawesome-webfont" src="html/fonts/fontawesome-webfont.ttf" />
        <File name="fontawesome-webfont" src="html/fonts/fontawesome-webfont.woff" />
        <File name="fontawesome-webfont" src="html/fonts/fontawesome-webfont.woff2" />
        <File name="BG" src="html/img/BG.jpg" />
        <File name="FSCloseBtn" src="html/img/FSCloseBtn.png" />
        <File name="dance" src="html/img/btn/dance.jpg" />
        <File name="down" src="html/img/btn/down.jpg" />
        <File name="game" src="html/img/btn/game.jpg" />
        <File name="home" src="html/img/btn/home.jpg" />
        <File name="lastestevent" src="html/img/btn/lastestevent.jpg" />
        <File name="latestevent" src="html/img/btn/latestevent.jpg" />
        <File name="latestpromotion" src="html/img/btn/latestpromotion.jpg" />
        <File name="membership" src="html/img/btn/membership.jpg" />
        <File name="pose" src="html/img/btn/pose.jpg" />
        <File name="rpsgame" src="html/img/btn/rpsgame.jpg" />
        <File name="up" src="html/img/btn/up.jpg" />
        <File name="face" src="html/img/face.png" />
        <File name="face1" src="html/img/face1.png" />
        <File name="imgConfig" src="html/img/imgConfig.js" />
        <File name="1" src="html/img/main/Activity/1.jpg" />
        <File name="2" src="html/img/main/Activity/2.jpg" />
        <File name="3" src="html/img/main/Activity/3.jpg" />
        <File name="4" src="html/img/main/Activity/4.jpg" />
        <File name="5" src="html/img/main/Activity/5.jpg" />
        <File name="6" src="html/img/main/Activity/6.jpg" />
        <File name="1" src="html/img/main/Announcement/1.jpg" />
        <File name="2" src="html/img/main/Announcement/2.jpg" />
        <File name="CityRewards_clubs_v01_03_1" src="html/img/main/CityRewards_clubs_v01_03_1.jpg" />
        <File name="Nakata" src="html/img/main/Nakata.jpg" />
        <File name="Park Signature content page_2 968x606-01" src="html/img/main/Park Signature content page_2 968x606-01.jpg" />
        <File name="Park Signature content page_2 968x606-02" src="html/img/main/Park Signature content page_2 968x606-02.jpg" />
        <File name="Park Signature content page_2 968x606-03" src="html/img/main/Park Signature content page_2 968x606-03.jpg" />
        <File name="Park Signature content page_2 968x606-04" src="html/img/main/Park Signature content page_2 968x606-04.jpg" />
        <File name="Park Signature content page_2 968x606-05" src="html/img/main/Park Signature content page_2 968x606-05.jpg" />
        <File name="Park Signature content page_2 968x606-06" src="html/img/main/Park Signature content page_2 968x606-06.jpg" />
        <File name="1 - Copy" src="html/img/main/QRcode/1 - Copy.jpg" />
        <File name="1" src="html/img/main/QRcode/1.jpg" />
        <File name="1" src="html/img/main/TradePlatform/1.jpg" />
        <File name="2" src="html/img/main/TradePlatform/2.jpg" />
        <File name="3" src="html/img/main/TradePlatform/3.jpg" />
        <File name="4" src="html/img/main/TradePlatform/4.jpg" />
        <File name="cod-baccarat-tournament-2018-700x300" src="html/img/main/cod-baccarat-tournament-2018-700x300.jpg" />
        <File name="cover" src="html/img/main/cover.jpg" />
        <File name="dance" src="html/img/main/dance.jpg" />
        <File name="direct-booking1920x980_V3" src="html/img/main/direct-booking1920x980_V3.jpg" />
        <File name="down" src="html/img/main/down.jpg" />
        <File name="game" src="html/img/main/game.jpg" />
        <File name="home" src="html/img/main/home.jpg" />
        <File name="iamnew" src="html/img/main/iamnew.jpg" />
        <File name="iamnewbtn" src="html/img/main/iamnewbtn.jpg" />
        <File name="lastestevent" src="html/img/main/lastestevent.jpg" />
        <File name="latestevent" src="html/img/main/latestevent.jpg" />
        <File name="latestpromotion" src="html/img/main/latestpromotion.jpg" />
        <File name="main" src="html/img/main/main.jpg" />
        <File name="membership" src="html/img/main/membership.jpg" />
        <File name="membership1" src="html/img/main/membership1.jpg" />
        <File name="membership2" src="html/img/main/membership2.jpg" />
        <File name="membership3" src="html/img/main/membership3.jpg" />
        <File name="membership4" src="html/img/main/membership4.jpg" />
        <File name="nuwa-spa-elemental-stone-series640x500" src="html/img/main/nuwa-spa-elemental-stone-series640x500.jpg" />
        <File name="offer1" src="html/img/main/offer1.jpg" />
        <File name="offer2" src="html/img/main/offer2.jpg" />
        <File name="offer3" src="html/img/main/offer3.jpg" />
        <File name="offer4" src="html/img/main/offer4.jpg" />
        <File name="offer5" src="html/img/main/offer5.jpg" />
        <File name="offer6" src="html/img/main/offer6.jpg" />
        <File name="p1" src="html/img/main/p1.png" />
        <File name="pose" src="html/img/main/pose.jpg" />
        <File name="rpsgame" src="html/img/main/rpsgame.jpg" />
        <File name="slideleft" src="html/img/main/slideleft.jpg" />
        <File name="slideright" src="html/img/main/slideright.jpg" />
        <File name="slideup" src="html/img/main/slideup.jpg" />
        <File name="thodw2018-1920x480" src="html/img/main/thodw2018-1920x480.jpg" />
        <File name="transparent" src="html/img/main/transparent.png" />
        <File name="up" src="html/img/main/up.jpg" />
        <File name="menu_option1" src="html/img/menu_option1.png" />
        <File name="menu_option2" src="html/img/menu_option2.png" />
        <File name="menu_option2_old" src="html/img/menu_option2_old.png" />
        <File name="recording" src="html/img/recording.gif" />
        <File name="index" src="html/index.html" />
        <File name="angular-PapaParse" src="html/js/angular-PapaParse.js" />
        <File name="angular-PapaParse.min" src="html/js/angular-PapaParse.min.js" />
        <File name="angular-fastclick.min" src="html/js/angular-fastclick.min.js" />
        <File name="angular-route.min" src="html/js/angular-route.min.js" />
        <File name="angular-touch" src="html/js/angular-touch.js" />
        <File name="angular-touch.min" src="html/js/angular-touch.min.js" />
        <File name="angular.min" src="html/js/angular.min.js" />
        <File name="awesomplete" src="html/js/awesomplete.js" />
        <File name="awesomplete.min" src="html/js/awesomplete.min.js" />
        <File name="bootstrap" src="html/js/bootstrap.js" />
        <File name="bootstrap.min" src="html/js/bootstrap.min.js" />
        <File name="jquery-1.11.0.min" src="html/js/jquery-1.11.0.min.js" />
        <File name="jquery-2.1.4.min" src="html/js/jquery-2.1.4.min.js" />
        <File name="jquery-3.1.1.min" src="html/js/jquery-3.1.1.min.js" />
        <File name="jquery.mobile.custom.min" src="html/js/jquery.mobile.custom.min.js" />
        <File name="js.cookie" src="html/js/js.cookie.js" />
        <File name="main" src="html/js/main.js" />
        <File name="main_camera" src="html/js/main_camera.js" />
        <File name="moment.min" src="html/js/moment.min.js" />
        <File name="npm" src="html/js/npm.js" />
        <File name="papaparse.min" src="html/js/papaparse.min.js" />
        <File name="plyr" src="html/js/plyr.js" />
        <File name="robotutils.1.0" src="html/js/robotutils.1.0.js" />
        <File name="robotutils" src="html/js/robotutils.js" />
        <File name="robotutils.min" src="html/js/robotutils.min.js" />
        <File name="robotutils_2.0" src="html/js/robotutils_2.0.js" />
        <File name="videoutils" src="html/js/videoutils.js" />
        <File name="videoutils.min" src="html/js/videoutils.min.js" />
        <File name="sample" src="html/json_data/sample.json" />
        <File name="01_Hungary" src="hungdance/01_Hungary.mp3" />
        <File name="02_Hungary" src="hungdance/02_Hungary.ogg" />
        <File name="main" src="main.png" />
        <File name="README" src="script/butane/README" />
        <File name="__init__" src="script/butane/__init__.py" />
        <File name="conversation" src="script/butane/conversation.py" />
        <File name="fuel" src="script/butane/fuel.py" />
        <File name="language_utils" src="script/butane/language_utils.py" />
        <File name="package_utils" src="script/butane/package_utils.py" />
        <File name="main" src="script/main.py" />
        <File name="main_old" src="script/main_old.py" />
        <File name="pr_promo_code" src="script/pr_promo_code.py" />
        <File name="__init__" src="script/stk/__init__.py" />
        <File name="__init__" src="script/stk/__init__.pyc" />
        <File name="events" src="script/stk/events.py" />
        <File name="events" src="script/stk/events.pyc" />
        <File name="logging" src="script/stk/logging.py" />
        <File name="logging" src="script/stk/logging.pyc" />
        <File name="runner" src="script/stk/runner.py" />
        <File name="runner" src="script/stk/runner.pyc" />
        <File name="services" src="script/stk/services.py" />
        <File name="services" src="script/stk/services.pyc" />
        <File name="teks_configure" src="script/teks_configure.py" />
        <File name="teksphotolib" src="script/teksphotolib.py" />
        <File name="translation_en_US" src="translations/translation_en_US.qm" />
        <File name="translation_zh_TW" src="translations/translation_zh_TW.qm" />
    </Resources>
    <Topics>
        <Topic name="ExampleDialog_enu" src="main/ExampleDialog/ExampleDialog_enu.top" topicName="ExampleDialog" language="en_US" />
        <Topic name="lexicon_enu" src="say/lexicon_enu.top" topicName="lexicon" language="en_US" />
        <Topic name="say_enu" src="say/say_enu.top" topicName="say" language="en_US" />
        <Topic name="Lexicon_enu" src="script/butane/dialog/Lexicon/Lexicon_enu.top" topicName="Lexicon" language="en_US" />
    </Topics>
    <IgnoredPaths />
    <Translations auto-fill="en_US">
        <Translation name="translation_en_US" src="translations/translation_en_US.ts" language="en_US" />
        <Translation name="translation_zh_TW" src="translations/translation_zh_TW.ts" language="zh_TW" />
    </Translations>
</Package>
