<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US">
    <context>
        <name>behavior.xar:/Get Localized Text</name>
        <message>
            <source>Hello</source>
            <comment>Text</comment>
            <translation type="vanished">Hello</translation>
        </message>
        <message>
            <source>test</source>
            <comment>Text</comment>
            <translation type="obsolete">test</translation>
        </message>
        <message>
            <source>feedback</source>
            <comment>Text</comment>
            <translation type="obsolete">feedback</translation>
        </message>
    </context>
    <context>
        <name>feedback/main/behavior.xar:/Get Localized Text</name>
        <message>
            <source>feedback</source>
            <comment>Text</comment>
            <translation type="obsolete">feedback</translation>
        </message>
    </context>
    <context>
        <name>main/behavior.xar:/Get Localized Text</name>
        <message>
            <location filename="main/behavior.xar" line="0"/>
            <source>feedback</source>
            <comment>Text</comment>
            <translation type="unfinished">feedback</translation>
        </message>
    </context>
    <context>
        <name>main/behavior.xar:/Get Localized Text (1)</name>
        <message>
            <source>Hello</source>
            <comment>Text</comment>
            <translation type="vanished">Hello</translation>
        </message>
        <message>
            <source>feedback</source>
            <comment>Text</comment>
            <translation type="obsolete">feedback</translation>
        </message>
        <message>
            <source>你好</source>
            <comment>Text</comment>
            <translation type="obsolete">你好</translation>
        </message>
        <message>
            <source>hello. I am running</source>
            <comment>Text</comment>
            <translation type="obsolete">hello. I am running</translation>
        </message>
    </context>
    <context>
        <name>main/behavior.xar:/Get Localized Text (2)</name>
        <message>
            <source>feedback</source>
            <comment>Text</comment>
            <translation type="obsolete">feedback</translation>
        </message>
    </context>
</TS>
