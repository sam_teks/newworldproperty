#!/usr/bin/env python
# -*- coding: utf-8 -*-

import ConfigParser
import os
import teks_authorize_neo4j as authorize_neo4j
import teks_dialog_configure as CONFIGURE

###############################################################################


def search_answer(graph, question, logger, language = 'CT'):
    lang = language
    question_text = question.encode("utf-8")
    search_node_sentence = CONFIGURE.SEARCH_NODE_SENTENCE
    result = None
    file_name = CONFIGURE.FILE_NAME
    print "file name: " + str(file_name)
    print "logger: " + str(logger)
    domain = getDomain(file_name, logger)
    print "domian" + str(domain)
    # if lang == 'CT':
    #     try:
    #         result = graph.data(search_node_sentence.format(domain, lang, question_text))
    #         logger.info('---CT Success---while searching')
    #     except RuntimeError as err:
    #         logger.error('---CT error---while searching')
    try:
        print "domain" + str(domain)
        print "lang" + str(lang)
        print "question_text" + str(question_text)
        result = graph.data(search_node_sentence.format(domain, lang, question_text))
        logger.info('---CT Success---while searching')
    except RuntimeError as err:
        logger.error('---CT error---while searching')
    # elif lang == 'CN':
    #     try:
    #         result = graph.data(search_node_sentence.format(domain, lang, question_text))
    #         logger.info('---CN Success---while searching')
    #     except RuntimeError as err:
    #         logger.error('---CN error---while searching')
    # elif lang == "EN":
    #     try:
    #         result = graph.data(search_node_sentence.format(domain, lang, question_text))
    #         logger.info('---EN Success---while searching')
    #     except RuntimeError as err:
    #         logger.error('---EN error---while searching')
    else:
        logger.error('---Language error---while searching')
    if not result:
        return ""
    else:
        return result[0]['Answer']

################################################################################


def insert_QA(graph, question, logger, language = 'CT',answer="唔好意思,  請向我地服務員查詢", seeded="1"):
    lang = language
    answer_text = answer.encode("utf-8")
    question_text = question.encode("utf-8")
    seed = seeded
    result = None
    file_name = CONFIGURE.FILE_NAME
    domain = getDomain(file_name, logger)
    subCaption = domain.strip(lang+"FAQ")+"QNA"

    if lang == 'CT':
        try:
            result = graph.data(CONFIGURE.SEARCH_NODE_SENTENCE.format(domain, lang, question_text))
            if not result:
                graph.data(CONFIGURE.INSERT_NODE_SENTENCE %
                           (domain, subCaption, lang, question_text, answer_text, seed))
                logger.info('----create OK-----CT')
        except Exception as e:
            logger.error(e)
            logger.error('---CT error---while inserting')
    elif lang == 'CN':
        try:
            result = graph.data(CONFIGURE.SEARCH_NODE_SENTENCE.format(domain, lang, question_text))
            logger.info(result)
            if not result:
                graph.data(CONFIGURE.INSERT_NODE_SENTENCE %
                           (domain, subCaption, lang, question_text, answer_text, seed))
                logger.info('----create OK-----CN')
        except Exception as e:
            logger.error(e)
            logger.error('---CN error---while inserting')
    elif lang == 'EN':
        try:
            result = graph.data(CONFIGURE.SEARCH_NODE_SENTENCE.format(domain, lang, question_text))
            if not result:
                graph.data(CONFIGURE.INSERT_NODE_SENTENCE %
                           (domain, subCaption, lang, question_text, answer_text, seed))

                logger.info('----create OK-----EN')
        except Exception as e:
            logger.error(e)
            logger.error('---EN error---while inserting')
    else:
        logger.error('---language error---while inserting')

    pass


def getDomain(file_name, logger):
    cp = ConfigParser.ConfigParser()
    cp.read(file_name)
    # logger.info(os.path.exists('app_config.ini'))
    # logger.info(cp.sections())
    try:
        domain = cp.get('neo4j','domain')
    except Exception as e:
        print("I cannot get the domain")
    # print 'neo4j domain: ',domain
    return domain
