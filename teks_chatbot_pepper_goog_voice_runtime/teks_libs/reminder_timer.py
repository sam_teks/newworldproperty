class ReminderTimer(object):
    def __init__(self, service_cache):
        self.s = service_cache

        self.suuspend_flag = False

        self.reminder_count = 0
        self.reminder_expiry = 6
        self.reminder_msgs=[
                            "",
                            "Please tell me your selection",
                            "Are you there, Please tell me your selection",
                            "find anything you like, are you still around",
                            "I will end your session in 10 seconds, please place your order",
                            "I am ending your session for your security, please contact my other colleague if any question"
                            ]

        self.expiryCB = None

    def set_messages(self,msgs):
        self.reminder_msgs = msgs
        self.reminder_expiry = len(self.reminder_msgs)
        pass

    def reset(self):
        self.reminder_count = 0
        pass

    def stop(self):
        self.suuspend_flag = True
        pass

    def start(self):
        self.suuspend_flag = False
        pass

    def set_expiryCallback(self,cb):
        self.expiryCB = cb
        pass

    def raise_reminder(self):
        msg = None
        if not self.suuspend_flag and self.reminder_count < self.reminder_expiry:
            msg = self.reminder_msgs[self.reminder_count]
            self.reminder_count += 1

            self.s.ALAnimatedSpeech.say(msg)

        if self.reminder_count >= self.reminder_expiry and self.expiryCB:
            self.expiryCB()

        return msg

