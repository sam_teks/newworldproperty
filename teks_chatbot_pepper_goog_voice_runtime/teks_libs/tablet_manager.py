import time

class TabletManager(object):

    def __init__(self, s, rlock, appname):

        self.s = s
        self.tablet = self.s.ALTabletService

        self.rLock = rlock
        self.app_name = appname

        self.sleeptime = 0.5

        self.activeCB = self.cancelledCB = self.timeoutCB = None

        self._isRunning = False

        pass

    @property
    def isRunning(self):
        return self._isRunning

    @isRunning.setter
    def isRunning(self, value):
        self._isRunning = value

    def show(self, url=None):
        if not self.isRunning:
            if url:
                self.tablet.showWebview(url)
            else:
                print("loadApplication")
                self.tablet.loadApplication(self.app_name)
                self.tablet.showWebview()
                self.isRunning = True
        pass

    def hide(self):
        if self.isRunning:
            self.tablet.hideWebview()
            self.isRunning = False

    def restart(self, url=None):
        self.hide()
        time.sleep(2.0)
        self.show(url)
        time.sleep(2.0)

    def isRunning(self):
        return self.isRunning()

    def setActiveCallback(self, cb):
        self.activeCB = cb
        pass

    def setCancelledCallback(self, cb):
        self.cancelledCB = cb
        pass

    def setTimeoutCallback(self, cb):
        self.timeoutCB = cb
        pass

