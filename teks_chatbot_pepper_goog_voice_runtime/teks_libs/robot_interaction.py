import time

STAND_UP_DURATION = 0.3

class RobotInteraction(object):

    def __init__(self, service_cache):
        #self.language = self.s.ALDialog.getLanguage()
        self.s = service_cache

    def set_external_collision(self, parts="Arms", enable=True):
        self.s.ALMotion.setExternalCollisionProtectionEnabled(parts,enable)

    def start_headstop_behavior(self, behavior):
        # if self.s.ALBehaviorManager.isBehaviorRunning(SCENE_BEHAVIORS[int(video)]):
        #     self.s.ALBehaviorManager.stopBehavior(SCENE_BEHAVIORS[int(video)])
        # self.s.ALBehaviorManager.startBehavior(behavior)
        # self.logger.info("started "+behavior)
        # self.events.connect("FrontTactilTouched", self.set_touch_sigal)
        # while self.s.ALBehaviorManager.isBehaviorRunning(behavior):
        #     if self.head_touched:
        #         self.s.ALBehaviorManager.stopBehavior(behavior)
        #         self.logger.info("stopped "+behavior)
        #         self.head_touched = False
        # self.events.disconnect("FrontTactilTouched")
        # self.logger.info("stopped "+behavior)
        pass
        
    def stand_up(self, data):
        self.s.ALRobotPosture.goToPosture("Stand", STAND_UP_DURATION)

    def speech_volume(self, data):
        try:
            if data and len(data) > 1 and (data[0] == "d" or data[0] == "i"):
                #val = float(self.s.ALTextToSpeech.getParameter("volume"))
                val = float(self.s.ALAudioDevice.getOutputVolume())
                adj = float(data[1:])
                if data[0] == "i":
                    if val+adj <= 100:
                        self.s.ALAudioDevice.setOutputVolume(val+adj)
                elif data[0] == "d":
                    if val-adj >= 0:
                        self.s.ALAudioDevice.setOutputVolume(val-adj)
        except Exception as e:
            print(str(e))
            pass

    def speech_speed(self, data):
        print("speech_speed: "+str(data))
        try:
            if data and len(data) > 1 and (data[0] == "d" or data[0] == "i"):
                val = float(self.s.ALTextToSpeech.getParameter("speed"))
                adj = float(data[1:])
                print("speech_speed: "+str(val)+data[0]+str(adj))
                if data[0] == "i":
                    if val+adj <= 400:
                        self.s.ALTextToSpeech.setParameter("speed",val+adj)
                elif data[0] == "d":
                    if val-adj >= 50:
                        self.s.ALTextToSpeech.setParameter("speed",val-adj)
        except Exception as e:
            print(str(e))
            pass

