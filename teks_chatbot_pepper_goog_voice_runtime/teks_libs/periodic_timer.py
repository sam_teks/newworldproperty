import time
import threading

class PeriodicTimer(threading.Thread):

    def __init__(self, stop_event, rlock, interval=1):
        threading.Thread.__init__(self)

        self.thread_stop_event = stop_event
        self.thread_rlock = rlock
        
        self.thread_stop = False
        self.suspend_flag = True

        self.status = None

        self.activeCB = self.cancelledCB = self.timeoutCB = None

        # self.counter = 0

        # SECS = 1000000
        # self.CHECK_INTERVAL = int(interval * SECS)
        self.CHECK_INTERVAL = int(interval)

        # self.count_check_task = self.qi.PeriodicTask()
        # self.count_check_task.setCallback(self.check_timer)
        # self.count_check_task.setUsPeriod(self.CHECK_INTERVAL)

        pass

    def run(self):
        while not self.thread_stop_event.is_set():
            if self.suspend_flag:
                time.sleep(0.1)
                continue
               
            time.sleep(self.CHECK_INTERVAL)     
            if not self.suspend_flag:
                if self.activeCB:
                    self.activeCB()

    def is_suspend(self):
        return self.suspend_flag

    def isRunning(self):
        return not self.suspend_flag

    def suspend(self):
        with self.thread_rlock:
            #print('suspend google beta')
            self.suspend_flag = True

    def stop(self):
        with self.thread_rlock:
            #print('suspend google beta')
            self.suspend_flag = True

    def resume(self):
        with self.thread_rlock:
            #print('resume google beta')
            self.suspend_flag = False

    # def start(self,immediate=True):
    #     with self.thread_rlock:
    #         #print('suspend google beta')
    #         self.suspend_flag = False
               
    def setInterval(self, value):
        with self.rLock:
            is_running = isRunning()
            if is_running:
                self.suspend_flag = True
            self.CHECK_INTERVAL = value
            if is_running:
                self.suspend_flag = False
        pass

    def setActiveCallback(self, cb):
        self.activeCB = cb
        pass

    def setCancelledCallback(self, cb):
        self.cancelledCB = cb
        pass

    def setTimeoutCallback(self, cb):
        self.timeoutCB = cb
        pass

