import time
import periodic_timer

from periodic_timer import *

class ClockTimer(object):

    def __init__(self, qi, rlock, maxCount=0, interval=1, step=1, pill2kill=None):
        self.qi = qi
        self.rLock = rlock

        self.sleeptime = 0.5
        self.status = None

        self.activeCB = self.cancelledCB = self.timeoutCB = None

        self.counter = 0
        self.maxCount = maxCount
        self.step = step

        # SECS = 1000000
        # self.CHECK_INTERVAL = int(interval * SECS)

        self.count_check_task = PeriodicTimer(pill2kill, rlock, interval=interval)
        self.count_check_task.setActiveCallback(self.check_timer)
        self.count_check_task.start()

        # self.count_check_task = self.qi.PeriodicTask()
        # self.count_check_task.setCallback(self.check_timer)
        # self.count_check_task.setUsPeriod(self.CHECK_INTERVAL)

        pass

    def start(self):
        print("in start")
        if not self.isRunning():
            print("starting")
            self.count_check_task.resume()
            print("end starting")

        #self.qi.async(self._get_code)
        pass

    def stop(self):
        with self.rLock:
            if self.isRunning():
                self.count_check_task.stop()

    def cancel(self):
        with self.rLock:
            if self.isRunning():
                self.count_check_task.stop()
            if self.cancelledCB:
                self.cancelledCB()

    def reset(self):
        print("in reset")
        with self.rLock:
            print("reseting")
            if self.isRunning():
                self.count_check_task.stop()
            self.counter = 0
            print("end reset")

    def restart(self):
        with self.rLock:
            self.reset()
            if not self.isRunning():
                self.count_check_task.resume()

    def isRunning(self):
        return self.count_check_task.isRunning()

    def setMaxCount(self, value):
        with self.rLock:
            if self.isRunning():
                self.count_check_task.stop()
            self.maxCount = value
            if not self.isRunning():
                self.count_check_task.resume()
        pass


    def setInterval(self, value):
        with self.rLock:
            if self.isRunning():
                self.count_check_task.stop()
            self.CHECK_INTERVAL = value
            #self.count_check_task.setUsPeriod(self.CHECK_INTERVAL)
            self.count_check_task.setInterval(self.CHECK_INTERVAL)
            if not self.isRunning():
                self.count_check_task.resume()
        pass


    def setActiveCallback(self, cb):
        self.activeCB = cb
        pass

    def setCancelledCallback(self, cb):
        self.cancelledCB = cb
        pass

    def setTimeoutCallback(self, cb):
        self.timeoutCB = cb
        pass

    def check_timer(self):
        print("timer clocking: "+str(self.counter))
        with self.rLock:
            if self.counter < self.maxCount:
                if self.activeCB:
                    self.activeCB()
            elif self.counter == self.maxCount:
                if self.timeoutCB:
                    self.timeoutCB()
                #self.stop()
            elif self.counter > self.maxCount:
                if self.timeoutCB:
                    self.timeoutCB()
                #self.stop()

            self.counter += self.step
        pass
