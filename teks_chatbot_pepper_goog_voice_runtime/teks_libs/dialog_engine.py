import time

class DialogEngine(object):

    def __init__(self, service_cache, logger, topic_names, pkg_path, subscriberID):
        #self.language = self.s.ALDialog.getLanguage()
        self.s = service_cache
        self.logger = logger
        self.topic_names = topic_names

        self.prev_topic_name = None

        self.subscriberID = subscriberID
        #self.TOPIC_NAME = 'MainMenu'
        #self.TOPIC_PATHS = {topic: '{0}/dialog/{1}/{1}_{2}.top'.format(pkg_path, topic, '{}') for topic in self.topic_names}
        self.TOPIC_PATHS = {topic: '{0}/dialog/mastercard/{1}_{2}.top'.format(pkg_path, topic, '{}') for topic in self.topic_names}
        #self.TOPIC_PATH = self.TOPIC_PATHS[self.TOPIC_NAME].format(code)
        self.language = "English"
        self.topic_lang=self.s.ALDialog.getLanguage()
        self.s.ALSpeechRecognition.setLanguage(self.language)
        self.s.ALTextToSpeech.setLanguage(self.language)
        self.s.ALDialog.setLanguage(self.language)

        self.ASRConfidenceThreshold = 0.505
        self.s.ALDialog.setASRConfidenceThreshold(self.ASRConfidenceThreshold)
        self.TOPIC_CONFIDENCE = {
            "add-on" : 0.505,
            "beverage" : 0.495,
            "category" : 0.5,
            "combo_product_item_holder" : 0.5,
            "combo_review" : 0.495,
            "crust" : 0.495,
            "hot_deal" : 0.495,
            "mastercard" : 0.5,
            "pincode" : 0.5,
            "pizza" : 0.495,
            "pizza_size" : 0.495,
            "process_trans" : 0.505,
            "quantity_addon" : 0.5,
            "quantity" : 0.5,
            "read_tag" : 0.505,
            "review_cart" : 0.495,
            "combo_side" : 0.495,
            "lcombo_side" : 0.495,
            "drink" : 0.495,
            "side" : 0.495
            }


        code = self.s.ALDialog.convertLongToNU(self.language)

        for topic in self.topic_names:
            #self.TOPIC_PATH = self.TOPIC_PATHS[topic].format('mnc')
            #self.s.ALDialog.loadTopic(self.TOPIC_PATH)
            self.TOPIC_PATH = self.TOPIC_PATHS[topic].format('enu')
            self.unload_topic(topic)
            self.s.ALDialog.loadTopic(self.TOPIC_PATH)

        try:
            self.s.ALDialog.compileAll()
            self.logger.info("compiled all the topics...")
        except Exception as e:
            self.logger.error(e)
        self.s.ALDialog.subscribe(self.subscriberID)

    def stop_speech(self):
        self.s.ALTextToSpeech.stopAll()
        self.s.ALAnimatedSpeech._stopAll(True)

    def stop_current_topic(self):
        self.s.ALTextToSpeech.stopAll()
        self.s.ALAnimatedSpeech._stopAll(True)

        try:
            self.s.ALDialog.unsubscribe(self.subscriberID)
        except:
            self.logger.info("Cannot unsubscribe the dialog: "+self.subscriberID)

        # unload and reload new topics
        topics = self.s.ALDialog.getActivatedTopics()
        for topic in topics:
            print("==================deactivateTopic: "+topic)
            self.s.ALDialog.deactivateTopic(topic)

    def restartDialog(self, topic, force=False, ignore_same=False):
        #if same topic, no need to reload
        if ignore_same or self.prev_topic_name != topic:
            self.stop_current_topic()        
            self.startDialog(topic,force)
        self.prev_topic_name = topic

    def switch_topic(self, target_topic, force=True, ignore_same=True):
        self.s.ALTextToSpeech.stopAll()
        self.s.ALAnimatedSpeech._stopAll(True)
        
        #if ignore_same or self.prev_topic_name != target_topic:
        if ignore_same:
            try:
                topics = self.s.ALDialog.getActivatedTopics()
                for topic in topics:
                    print("==================deactivateTopic: "+topic)
                    self.s.ALDialog.deactivateTopic(topic)
                self.s.ALDialog.activateTopic(target_topic)
                print("==================activateTopic: "+target_topic)
                self.s.ALDialog.setFocus(target_topic)
                if force:
                    self.s.ALDialog.forceOutput()
            except Exception as e:
                self.logger.warning(e)
        self.prev_topic_name = target_topic
        if target_topic in self.TOPIC_CONFIDENCE:
            target_threshold = self.TOPIC_CONFIDENCE[target_topic]
            print("============== target target_threshold : "+ str(target_threshold))
            self.s.ALDialog.setASRConfidenceThreshold(target_threshold)
        else:
            self.s.ALDialog.setASRConfidenceThreshold(self.ASRConfidenceThreshold)

    def startDialog(self, topic, force=False):
        try:
            self.s.ALDialog.activateTopic(topic)
            print("==================activateTopic: "+topic)
            self.s.ALDialog.setFocus(topic)
            self.s.ALDialog.subscribe(self.subscriberID)
            if force:
                self.s.ALDialog.forceOutput()
        except Exception as e:
            self.logger.warning(e)

    def stopDialog(self):
        self.stop_current_topic()
        #for topic in self.s.ALDialog.getLoadedTopics("Chinese"):
        #    self.unload_topic(topic)
        for topic in self.s.ALDialog.getActivatedTopics():
            self.unload_topic(topic)

    def deactivate_topic(self, topic):
        try:
            self.s.ALDialog.deactivateTopic(topic)
        except Exception as e:
            self.logger.warning("ERROR {}".format(e))
        pass

    def unload_topic(self, topic):
        try:
            self.s.ALDialog.deactivateTopic(topic)
            self.logger.info("Topic {} deactivated".format(topic))
            self.s.ALDialog.unloadTopic(topic, _async=True)
            self.logger.info("Topic {} unloaded".format(topic))
        except Exception as e:
            self.logger.warning("ERROR {}".format(e))
