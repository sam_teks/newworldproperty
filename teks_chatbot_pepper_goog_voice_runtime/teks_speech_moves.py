#!/usr/bin/python
# -*- coding: utf-8 -*-

############################################################

import sys
import time
import ctypes

from naoqi import ALProxy
from naoqi import ALBroker
import qi

import teks_configure as CONFIGURE

import random

################################################################################

class teks_speech_moves(object):
    def __init__(self):

        self.bmProxy = ALProxy("ALBehaviorManager", CONFIGURE.NAO_ALPROXY_IP, CONFIGURE.NAO_ALPROXY_PORT)
        self.postureProxy = ALProxy("ALRobotPosture", CONFIGURE.NAO_ALPROXY_IP, CONFIGURE.NAO_ALPROXY_PORT)

        self.behavior_list = [
                              "Stand/BodyTalk/Speaking/BodyTalk_2",
                              "Stand/BodyTalk/Speaking/BodyTalk_3",
                              "Stand/BodyTalk/Speaking/BodyTalk_4",
                              "Stand/BodyTalk/Speaking/BodyTalk_5",
                              "Stand/BodyTalk/Speaking/BodyTalk_6",
                              "Stand/BodyTalk/Speaking/BodyTalk_7",
                              "Stand/BodyTalk/Speaking/BodyTalk_8",
                              "Stand/BodyTalk/Speaking/BodyTalk_9",
                              "Stand/BodyTalk/Speaking/BodyTalk_10",
                              "Stand/BodyTalk/Speaking/BodyTalk_11",
                              "Stand/BodyTalk/Speaking/BodyTalk_12",
                              "Stand/BodyTalk/Speaking/BodyTalk_14",
                              "Stand/BodyTalk/Speaking/BodyTalk_15",
                              "Stand/BodyTalk/Speaking/BodyTalk_16"]

    def __def__(self):
        #ttsso.ttsLogOut()
        pass

    def start_move(self):
        random.seed(time.time())
        self.selected_behavior = random.choice(self.behavior_list)
        if self.bmProxy:
            if not self.bmProxy.isBehaviorRunning(self.selected_behavior):
                self.bmProxy.runBehavior(self.selected_behavior)


    def stop_move(self):
        self.postureProxy.post.goToPosture("Stand", 7.0)


################################################################################

from teks_my_thread import teks_thread

class teks_speech_moves_thread(teks_thread):
    def __init__(self, stop_event, rlock):
        teks_thread.__init__(self, stop_event, rlock)

        self.moves = teks_speech_moves()

    def suspend(self):
        with self.thread_rlock:
            self.suspend_flag = True
            self.moves.stop_move()

    def run(self):
        while not self.thread_stop_event.is_set():
            if self.suspend_flag:
                time.sleep(0.3)
                continue

            # do something
            self.moves.start_move()

################################################################################

import threading

if __name__ == "__main__":

    pill2kill = threading.Event()
    rlock = threading.RLock()

    move_thread = teks_speech_moves_thread(pill2kill, rlock)
    move_thread.start()

    pill2kill.set()
    tts_thread.join()
