#!/usr/bin/env python
#  -*- coding:utf-8 -*-

import os
import time

import teks_configure as CONFIGURE
import teks_brain_process as brain_process

################################################################################

from teks_my_thread import teks_thread

class teks_watch_dog_thread(teks_thread):

  def __init__(self, stop_event, rlock):
    teks_thread.__init__(self, stop_event, rlock)

    self.rivescript_brain_modified_time_dict = brain_process.get_brain_modified_time()

    self.suspend_flag = False

    self.modified_flag = False
    self.cur_modified_flag = False

  def is_modified(self):
    return self.modified_flag

  def set_modified_flag(self, flag):
    self.modified_flag = flag

  def reset_modified_flag(self):
    self.modified_flag = False

  def check_rivescript_brain_path_modified(self):
    return brain_process.check_brain_modified_time(self.rivescript_brain_modified_time_dict)

  def get_rivescript_brain_path(self):
    return brain_process.get_brain_path()

  def update_rivescript_brain_path_info(self):
    self.rivescript_brain_modified_time_dict = brain_process.get_brain_modified_time()

  def run(self):

    while not self.thread_stop_event.is_set():
      if self.suspend_flag:
        time.sleep(1.0)
        continue

      time.sleep(5.0)
      self.cur_modified_flag = self.check_rivescript_brain_path_modified()
      # if change to true, will hold the status until the flag is changed / reset manually.
      if self.cur_modified_flag:
          print "++++ update ++++"
          self.modified_flag = self.cur_modified_flag

      # do something

      #self.suspend_flag = True

################################################################################

import threading

if __name__ == "__main__":

  pill2kill = threading.Event()
  rlock = threading.RLock()

  watch_dog_thread = teks_watch_dog_thread(pill2kill, rlock)
  watch_dog_thread.start()
  watch_dog_thread.set_name("I/am/watch/dog")
  print watch_dog_thread.get_name()

  try:
    while True:
      try:
        watch_dog_thread.resume()
        while not watch_dog_thread.is_suspend(): time.sleep(0.01)
        watch_dog_thread.suspend()

        modified_flag = watch_dog_thread.check_rivescript_brain_path_modified()
        if modified_flag:
          print "YOU! MODIFIED!"
          watch_dog_thread.update_rivescript_brain_path_info()
        else:
          print "NOOO MODIFIED!"
        time.sleep(1.0)

      except Exception as e:
          print e
          pill2kill.set()
          pass

  except Exception as e:
      print e

  finally:
      print '\n*** WAIT FOR CHILD THREADS EXIT ***\n'

      pill2kill.set()
      watch_dog_thread.join()

      print '\n*** FINISH ***\n'
