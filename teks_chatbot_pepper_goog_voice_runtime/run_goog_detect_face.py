#!/usr/bin/env python
#  -*- coding:utf-8 -*-

import os
import subprocess
import sys
import time
import json
import simplejson
import requests
import urllib2
import urllib
import threading
import teks_configure as CONFIGURE
import datetime
from teks_utility import *
#from teks_asr_ifly import teks_record_thread
from teks_asr_goog_beta import teks_record_thread
from teks_tts import teks_tts_thread
from teks_led_state import led_state
from teks_audio_player import audio_player
import neo4jtools.teks_neo4j_func as db_tools
import neo4jtools.teks_authorize_neo4j as neo4j_graph
from teks_speech_moves import teks_speech_moves_thread
from secret_words import *

from teks_omba_utils import omba_processor
import teks_brain_process as brain_process
from teks_watch_dog import teks_watch_dog_thread
from teks_watch_dog_dcm import teks_watch_dog_dcm_thread
# from getUtils import ctDigit2Digit

import math

import logging
import teks_logger as teks_log

from teks_libs.clock_timer import *

DEFAULT_LOGGER_FORMATTER = logging.Formatter('%(message)s,%(levelname)s,"%(asctime)s"')
DEFAULT_LOGGER_LEVEL = logging.INFO

logger = teks_log.teks_logger("teks_demo", \
                     DEFAULT_LOGGER_FORMATTER, \
                     None, \
                     DEFAULT_LOGGER_LEVEL)

from naoqi import ALProxy
from naoqi import ALModule
from naoqi import ALBroker
import qi

from os.path import isfile, join, splitext

################################################################################

app_starter = None
event_starter = None
loc_name = None

UNMATCHED_SENTENCE_LIST = []
CURRENT_LANGUAGE = CONFIGURE.LANG_CT
#CURRENT_LANGUAGE = CONFIGURE.LANG_CN

pill2kill = threading.Event()
rlock = threading.RLock()
last_recognized_face = None
last_recognized_face_match_level = 0
ttsProxy = None
record_thread = None
runningApp = ""
recordingAudio = False

StartTime=time.time()

def action() :
    print('action ! -> time : {:.1f}s'.format(time.time()-StartTime))

class setInterval :
    def __init__(self,interval,action) :
        self.interval=interval
        self.action=action
        self.stopEvent=threading.Event()
        thread=threading.Thread(target=self.__setInterval)
        thread.start()

    def __setInterval(self) :
        nextTime=time.time()+self.interval
        while not self.stopEvent.wait(nextTime-time.time()) :
            nextTime+=self.interval
            self.action()

    def cancel(self) :
        self.stopEvent.set()

class ListeningControlModule(ALModule):
    detect_in = True 
    detectout_count = 0
    movement_now = False
    disable_recording = False
    numofface = 0
    question = 0
    customer_lang = CONFIGURE.LANG_CT

    # def __init__(self,ALModule):
    #     thread = threading.Thread(target=self.run, args=())
    #     thread.daemon = True                            # Daemonize thread
    #     thread.start()

    # def run(self):
    #     while True:
    #         # time.sleep(1)
    #         # start action every 0.6s
    #         # inter=setInterval(0.6,action)
    #         # print('just after setInterval -> time : {:.1f}s'.format(time.time()-StartTime))
    #         # add code
    #         print "running app" + str(runningApp)

    def onLoad(self):
        global pill2kill
        global rlock

        self.faceStopped = True
        #self.vc_thread = teks_video_capture.teks_video_capture_thread(pill2kill, rlock)
        pass

    '''def checkFace(self):
        global pill2kill
        global rlock

        self.vc_thread = teks_video_capture.teks_video_capture_thread(pill2kill, rlock)
        self.vc_thread.start()'''

    def setLock(self,rlock):
        self.rlock = rlock
        pass

    '''def SaySeen(self,name,value):
        global memProxy
        global CURRENT_LANGUAGE
        global last_recognized_face
        global last_recognized_face_match_level

        last_recognized_face = str(memProxy.getData("teks/lastRecognizedFace"))
        last_recognized_face_match_level = memProxy.getData("teks/lastRecognizedFaceMatchLevel")

        print(CURRENT_LANGUAGE)
        p = "hi I see you "+str(memProxy.getData("teks/lastRecognizedFace"))
        print(p)
        pass'''

    '''def SayMaybeSeen(self,name,value):
        global memProxy
        global CURRENT_LANGUAGE
        global last_recognized_face
        global last_recognized_face_match_level

        last_recognized_face = str(memProxy.getData("teks/lastRecognizedFace"))
        last_recognized_face_match_level = memProxy.getData("teks/lastRecognizedFaceMatchLevel")

        print(CURRENT_LANGUAGE)
        p = "hi mabye I see "+str(memProxy.getData("teks/lastRecognizedFace"))+" around"
        print(p)
        pass'''

    '''def foundFace(self,name,value):
        global event_starter
        global last_recognized_face
        global last_recognized_face_match_level
        global tts_thread
        global CURRENT_LANGUAGE

        with self.rlock:
            logging.info("foundFace called: ")
            if value: logging.info("foundFace value called: "+value)
            if value:
              jsonData = json.loads(value)
              last_recognized_face_match_level = jsonData["confidence"]
              if jsonData["confidence"] >= CONFIGURE.FACE_THRESHOLD:
                  print("match: "+jsonData["transcript"])
                  matchFile = str(jsonData["transcript"])
                  last_recognized_face = splitext(matchFile)[0]
                  #say_text(tts_thread, "I think I see you, "+splitext(matchFile)[0], CONFIGURE.LANG_EN)
                  #event_starter.start("teks/ResumeFaceDetect","1")
              elif jsonData["confidence"] >= CONFIGURE.FACE_THRESHOLD_2:
                  print("MAYBE match: "+jsonData["transcript"])
                  matchFile = str(jsonData["transcript"])
                  last_recognized_face = splitext(matchFile)[0]
                  #say_text(tts_thread, "Maybe I see, "+splitext(matchFile)[0]+" around", CONFIGURE.LANG_EN)
                  #event_starter.start("teks/ResumeFaceDetect","1")
              else:
                  print("no match")
                  with self.rlock:
                      if not self.faceStopped:
                          self.checkFace()
                      else:
                          event_starter.start("teks/ResumeFaceDetect","1")

        pass'''

    '''def notFoundFace(self,name,value):
        global event_starter
        global last_recognized_face

        logging.info("notFoundFace called: ")

        with self.rlock:
            if not self.faceStopped:
                last_recognized_face = None
                self.checkFace()
            else:
                event_starter.start("teks/ResumeFaceDetect","1")
        pass'''

    '''def startFace(self,name,value):
        global last_recognized_face
        logging.info("startFace called: ''''''''''''''''''''''''''''''''''''''''''''")

        #with self.rlock:
        #    self.faceStopped = False
        #    last_recognized_face = None
        #    self.checkFace()
        pass'''

    '''def stopFace(self,name,value):
        global last_recognized_face

        logging.info("stopFace called: ")
        with self.rlock:
            last_recognized_face = None
            self.faceStopped = True
        pass'''

    def faceDetected(self,name,value):
        if len(value):
            #print "~~~~~~~~~~~~~~~~~~~~~~~~ detect: len(p)="+str(len(value))+", len(p[1])="+str(len(value[1]))+" ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
            if (int(len(value[1]))-1) > self.numofface:
                self.numofface = int(len(value[1]))-1
        else:
            #print "~~~~~~~~~~~~~~~~~~~~~~~~ detect: len(p)="+str(len(value))+" ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
            self.numofface = 0
        pass

    def stopProcessText(self,name,value):
        global record_thread
        print "stopped processing text"
        ##logging.info("stopProcessText called: ")
        with self.rlock:
            if record_thread:
                ##logging.info("stopProcessText called suspend: ")
                record_thread.suspend_beh()
                event_starter.start("teks/tp_ct_c_newworldproperty/html_press_reject","1")
                event_starter.start("teks/tp_ct_c_newworldproperty/CallFunction","webStopRecord")
                event_starter.start("teks/tp_ct_c_newworldproperty/CallFunction","webDisableTouch")
        pass

    def startProcessText(self,name,value):
        global record_thread
        print "startProcessText = self.detect_in:"+str(self.detect_in)
        if self.detect_in == False:
            return

        if self.disable_recording:
            return

        ##logging.info("startProcessText called: ")
        with self.rlock:
            if record_thread:
                #time.sleep(0.5)
                ##logging.info("stopProcessText called resume:")
                record_thread.resume_beh()
                event_starter.start("teks/tp_ct_c_newworldproperty/html_press_allow","1")
                event_starter.start("teks/tp_ct_c_newworldproperty/CallFunction","webStartRecord")
                event_starter.start("teks/tp_ct_c_newworldproperty/CallFunction","webEnableTouch")
        pass

    def processText(self,lang_name,value):
        global omba_proc_ct
        global omba_proc_en
        global facility_list
        global facility_text_list
        global feedBackList
        global graph
        global tts
        global recordingAudio

        runningApp = memProxy.getData("runningApp")
        # recordingAudio = memProxy.getData("recordingAudio")

        if self.movement_now:
            print "self moving now"
            return

        if value==None or value=="":
            print "nothing heard"
            return

        print "{{{{{{{{{{{{{  ProcessText "+value +"received in lang "+ lang_name +"  }}}}}}}}}}}}}}}}}}}"
        event_starter.start("teks/Dialog/LastReceive",value)

        with self.rlock:
            #omba_proc = omba_proc_ct
            # if lang_name == "teks/tp_ct_c_newworldproperty/html":
            #     lang_name = CONFIGURE.LANG_CT
            #
            # if lang_name == CONFIGURE.LANG_CT:
            #     lang_name1 = check_language(value,lang_name)
            #     if lang_name == lang_name1:
            #         pass
            #     else:
            #         print "...STT listen CT, but language type: "+lang_name1
            #         lang_name = lang_name1
            #
            # elif lang_name == CONFIGURE.LANG_EN:
            #         lang_name =CONFIGURE.LANG_CT
            #     # lang_name1 = check_language(value,lang_name)
            #     # if lang_name == lang_name1:
            #     #     pass
            #     # else:
            #     #     print "...STT listen EN, but language type: "+lang_name1
            #     #     lang_name = lang_name1
            #
            # else:12
            #     pass

            lang_name = CONFIGURE.LANG_CT

            print "lang: " + lang_name
            print "configure ct:" +CONFIGURE.LANG_CT
            chk_foul_msg = value
            if foulwords_concept_ct(chk_foul_msg):
                logging.info("=====================Detected CT Foul Language========"+chk_foul_msg+"======")
                value = "四零四"
            if foulwords_concept_en(chk_foul_msg):
                logging.info("=====================Detected EN Foul Language========"+chk_foul_msg+"======")
                value = "four zero four"
            #process Chinese
            if lang_name == CONFIGURE.LANG_CT:
                print "runningApp: " + runningApp
                #Start rivescript behaviors
                if (True):
                    omba_proc_ct.bot.set_uservar(CONFIGURE.REQUEST_UID, "qrCode","")
                    omba_proc_ct.bot.set_uservar(CONFIGURE.REQUEST_UID, "behName","")
                    omba_proc_ct.bot.set_uservar(CONFIGURE.REQUEST_UID, "evtName","")
                    omba_proc_ct.bot.set_uservar(CONFIGURE.REQUEST_UID, "showImage","")
                    omba_proc_ct.bot.set_uservar(CONFIGURE.REQUEST_UID, "showPopup","")
                    omba_proc_ct.bot.set_uservar(CONFIGURE.REQUEST_UID, "showQuestion","")
                    omba_proc_ct.bot.set_uservar(CONFIGURE.REQUEST_UID, "htmlPop","")
                #start app specific config for processing text
                if runningApp == "facility" or runningApp == "feedback":
                    print "neo4j searching answers"
                    ans_in_yue = db_tools.search_answer(graph, value, logger, language='CT')

                    logging.info("==========neo4j answer=========")
                    print " >>>>>>   " + ans_in_yue.encode('utf-8')+"    <<<<<<<<<"
                    logging.info("==========neo4j answer=========")
                    # print "graph" +str(graph)
                    logging.warn('graph'+ str(graph))
                    print "value" +str(value)
                    print "logger" +str(logger)

                    if not ans_in_yue:
                        print "answer is not in data base"
                        # tts.say_text("对唔住，我仲要学习")
                        # ans_in_yue = "你可以發表意見 點擊以下鈕扣或者用語音觸發錄音"
                        try:
                            db_tools.insert_QA(graph, value, logger, language='CT', answer=ans_in_yue, seeded="1")
                        except RuntimeError as err:
                            logging.warning('Problem insert Q&A: {}'.format(err))
                    else:
                        print "answer is either facility or feedback"
                        if runningApp == "facility":
                            print "caught in the facility"
                            if ans_in_yue in facility_text_list:
                                # logging.info(ans_in_yue)
                                logging.info('you want to see the facility part')
                                idx = facility_text_list.index(ans_in_yue)
                                event_starter.start('teks/tp_ct_c_newworldproperty/showtext', idx)
                                event_starter.start('teks/tp_ct_c_newworldproperty/content_text', ans_in_yue.encode('utf-8'))
                            # tts.say_text(ans_in_yue.encode('utf-8'))
                            else:
                                ans_in_yue = "你可以查看設施資料 點擊以下按鈕或者用語音觸發開始"
                        elif runningApp == "feedback":
                            print "caught in the feedback"
                            if ans_in_yue == feedBackIntroduction:
                                print "caught in feedBackIntroduction" + feedBackIntroduction
                                reply = feedBackIntroduction
                            elif ans_in_yue in feedBackList:
                                # logging.info(ans_in_yue)
                                logging.info('you want to see the feedback')
                                event_starter.start("teks/tp_ct_c_newworldproperty_feedback/selectCategory", ans_in_yue.encode('utf-8'))

                                print "answer:" + ans_in_yue.encode('utf-8')
                                print "success before calling"
                                # recordingAudio =True
                                ans_in_yue = ans_in_yue;
                                # recordingAudio =                                memProxy.insertData("recordingAudio", True)
                                print "Setting recording Audio True"
                                #never start recording...
                                # event_starter.start("teks/tp_ct_c_newworldproperty_feedback/startrecording", ans_in_yue.encode('utf-8'))

                                print "succes calling"
                            else:
                                ans_in_yue = "你可以發表意見 點擊以下按鈕或者用語音觸發錄音"
                            # tts.say_text(ans_in_yue.encode('utf-8'))
                    reply = ans_in_yue;
                    print "reply ans"+ans_in_yue.encode('utf-8')
                else:
                    reply = omba_proc_ct.process_text(value.lower())
                #set the language again after rivescript process
                if reply!="零零零":
                    self.customer_lang = CONFIGURE.LANG_CT
                else:
                    reply=""
                # Start Rive script behavior
                if (True):
                    qrcode_name = omba_proc_ct.bot.get_uservar(CONFIGURE.REQUEST_UID, "qrCode")
                    beh_name = omba_proc_ct.bot.get_uservar(CONFIGURE.REQUEST_UID, "behName")
                    loc_name = omba_proc_ct.bot.get_uservar(CONFIGURE.REQUEST_UID, "targetloc")
                    page_name = omba_proc_ct.bot.get_uservar(CONFIGURE.REQUEST_UID, "pageName")
                    evt_name = omba_proc_ct.bot.get_uservar(CONFIGURE.REQUEST_UID, "evtName")
                    evt_param = omba_proc_ct.bot.get_uservar(CONFIGURE.REQUEST_UID, "evtParam")
                    showImage_name = omba_proc_ct.bot.get_uservar(CONFIGURE.REQUEST_UID, "showImage")
                    showPopup_name = omba_proc_ct.bot.get_uservar(CONFIGURE.REQUEST_UID, "showPopup")
                    showQuestion_name = omba_proc_ct.bot.get_uservar(CONFIGURE.REQUEST_UID, "showQuestion")
                    htmlPop_name = omba_proc_ct.bot.get_uservar(CONFIGURE.REQUEST_UID, "htmlPop")
            #Process English
            else:
                omba_proc_en.bot.set_uservar(CONFIGURE.REQUEST_UID, "qrCode","")
                omba_proc_en.bot.set_uservar(CONFIGURE.REQUEST_UID, "behName","")
                omba_proc_en.bot.set_uservar(CONFIGURE.REQUEST_UID, "evtName","")
                omba_proc_en.bot.set_uservar(CONFIGURE.REQUEST_UID, "showImage","")
                omba_proc_en.bot.set_uservar(CONFIGURE.REQUEST_UID, "showPopup","")
                omba_proc_en.bot.set_uservar(CONFIGURE.REQUEST_UID, "showQuestion","")
                omba_proc_en.bot.set_uservar(CONFIGURE.REQUEST_UID, "htmlPop","")

                reply = omba_proc_en.process_text(value.lower())

                if reply!="zero zero zero":
                    self.customer_lang = CONFIGURE.LANG_EN
                else:
                    reply=""

                qrcode_name = omba_proc_en.bot.get_uservar(CONFIGURE.REQUEST_UID, "qrCode")
                beh_name = omba_proc_en.bot.get_uservar(CONFIGURE.REQUEST_UID, "behName")
                loc_name = omba_proc_en.bot.get_uservar(CONFIGURE.REQUEST_UID, "targetloc")
                page_name = omba_proc_en.bot.get_uservar(CONFIGURE.REQUEST_UID, "pageName")
                evt_name = omba_proc_en.bot.get_uservar(CONFIGURE.REQUEST_UID, "evtName")
                evt_param = omba_proc_en.bot.get_uservar(CONFIGURE.REQUEST_UID, "evtParam")
                showImage_name = omba_proc_en.bot.get_uservar(CONFIGURE.REQUEST_UID, "showImage")
                showPopup_name = omba_proc_en.bot.get_uservar(CONFIGURE.REQUEST_UID, "showPopup")
                showQuestion_name = omba_proc_en.bot.get_uservar(CONFIGURE.REQUEST_UID, "showQuestion")
                htmlPop_name = omba_proc_en.bot.get_uservar(CONFIGURE.REQUEST_UID, "htmlPop")
            reply_text = reply.encode("utf8").strip()
            reply = ""
            if reply_text!="":
                self.stopProcessText(None,None)
            #print "reply_text: "+reply_text
            #--store to database
            db_log = teks_db_api.db_log_module("./teks_db/ui_counters.db")
            db_log.inc_counter_by_key("ui_counters",value,reply_text,str(datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')))
            db_log.close()
            #listenController.processText(name,value):
            event_starter.start("teks/Dialog/LastRespond",reply_text)

            if showImage_name and showImage_name != "undefined":
                omba_proc_ct.bot.set_uservar(CONFIGURE.REQUEST_UID, "showImage","")
                omba_proc_en.bot.set_uservar(CONFIGURE.REQUEST_UID, "showImage","")
                #omba_proc_cn.bot.set_uservar(CONFIGURE.REQUEST_UID, "showImage","")
                print "show Image raise " + showImage_name
                event_starter.start("teks/tp_ct_c_newworldproperty/CallFunction","goToSpecPos|'"+str(showImage_name)+"'")

            if showPopup_name and showPopup_name != "undefined":
                omba_proc_ct.bot.set_uservar(CONFIGURE.REQUEST_UID, "showPopup","")
                omba_proc_en.bot.set_uservar(CONFIGURE.REQUEST_UID, "showPopup","")
                #omba_proc_cn.bot.set_uservar(CONFIGURE.REQUEST_UID, "showPopup","")
                print "show Popup raise " + showPopup_name
                event_starter.start("teks/tp_ct_c_newworldproperty/CallFunction","goToSpecPopUp|'"+str(showPopup_name)+"'")

            if htmlPop_name and htmlPop_name != "undefined":
                omba_proc_ct.bot.set_uservar(CONFIGURE.REQUEST_UID, "htmlPop","")
                omba_proc_en.bot.set_uservar(CONFIGURE.REQUEST_UID, "htmlPop","")
                #if htmlPop_name == "exit":
                event_starter.start("teks/tp_ct_c_newworldproperty/CallFunction","cancelSpecPopUp")

            if qrcode_name and qrcode_name != "undefined":
                if qr_running == False:
                    qr_running = True
                    self.stopProcessText(None,None)

                    #---stop tracker and move reset head position
                    global trackerProxy
                    event_starter.start("teks/StandardChartered/FaceDetectOff","1")
                    motionProxy.setStiffnesses("Head", 1.0)
                    motionProxy.setAngles("HeadYaw",0,0.2)
                    motionProxy.setAngles("HeadPitch",-0.9,0.2)

                    #---start QR scan
                    print ">>>>>>>> qr_scan: start >>>>>>>>"
                    os.system("export LD_LIBRARY_PATH=/home/nao/.local/lib:$LD_LIBRARY_PATH; export PYTHONPATH=/home/nao/.local/lib/python2.7/site-packages:$PYTHONPATH; python /home/nao/.local/share/PackageManager/apps/citibank_pepper/script/main.py")
                    omba_proc_ct.bot.set_uservar(CONFIGURE.REQUEST_UID, "qrCode","")
                    omba_proc_en.bot.set_uservar(CONFIGURE.REQUEST_UID, "qrCode","")
                    #omba_proc_cn.bot.set_uservar(CONFIGURE.REQUEST_UID, "qrCode","")
                else:
                    print ">>>>>>>> qr_scan: already started >>>>>>>>"

            elif beh_name and beh_name != "undefined":
                #event_starter.start("teks/StartFaceDetect","1")
                print ">>>>>>>> behName = "+beh_name+" >>>>>>>>"
                omba_proc_ct.bot.set_uservar(CONFIGURE.REQUEST_UID, "behName","")
                omba_proc_en.bot.set_uservar(CONFIGURE.REQUEST_UID, "behName","")
                #omba_proc_cn.bot.set_uservar(CONFIGURE.REQUEST_UID, "behName","")

                logger.info('behName,'+beh_name)
                #logging.info("behName: "+beh_name)
                #logger.info('targetloc:'+loc_name)
                #logging.info("targetloc: "+loc_name)

                if reply_text:
                    led.eyes_fade(CONFIGURE.NAO_LED_COLOR_TTS_PROCESS)
                    #self.stopProcessText(None,None)
                    moves_thread.resume()
                    print "--------------------------------------------"
                    logger.info('reply_text,'+reply_text)
                    print "--------------------------------------------"
                    say_text(tts_thread, reply_text, lang_name)
                    moves_thread.suspend()
                    #self.startProcessText(None,None)
                    #event_starter.start("teks/tp_ct_c_newworldproperty/HandDown","1")

                #--long movement start
                if runningApp != "main":
                    listenController.movementStartProcess(None,None)    #alanykl, put this in Choregraphe will delay
                    time.sleep(0.5)

                print "START APPS"
                app_starter.start_aysn(str(beh_name))
                print "END START APPS"

            elif evt_name and evt_name != "undefined":
                omba_proc_ct.bot.set_uservar(CONFIGURE.REQUEST_UID, "evtName","")
                omba_proc_en.bot.set_uservar(CONFIGURE.REQUEST_UID, "evtName","")
                #omba_proc_cn.bot.set_uservar(CONFIGURE.REQUEST_UID, "evtName","")


                #print ">>>>>>>> evtName = "+evt_name+" - "+" >>>>>>>>"

                if reply_text:
                    led.eyes_fade(CONFIGURE.NAO_LED_COLOR_TTS_PROCESS)
                    #self.stopProcessText(None,None)
                    moves_thread.resume()
                    print "--------------------------------------------"
                    logger.info('reply_text,'+reply_text)
                    print "--------------------------------------------"
                    say_text(tts_thread, reply_text, lang_name)
                    moves_thread.suspend()
                    #time.sleep(1)

                print "EVENT NAME: " + str(evt_name)
                if evt_name!="teks/tp_ct_c_newworldproperty/html":
                    self.startProcessText(None,None)
                else:
                    print "~~~~~~~~~~~~event is html redirect"
                    self.stopProcessText(None,None)
                event_starter.start("teks/tp_ct_c_newworldproperty/HandDown","1")

                event_starter.start(evt_name,str(evt_param))

            elif showQuestion_name and showQuestion_name != "undefined":
                omba_proc_ct.bot.set_uservar(CONFIGURE.REQUEST_UID, "showQuestion","")
                omba_proc_en.bot.set_uservar(CONFIGURE.REQUEST_UID, "showQuestion","")
                #omba_proc_cn.bot.set_uservar(CONFIGURE.REQUEST_UID, "showQuestion","")

                if reply_text:
                    led.eyes_fade(CONFIGURE.NAO_LED_COLOR_TTS_PROCESS)
                    #self.stopProcessText(None,None)
                    moves_thread.resume()
                    print "--------------------------------------------"
                    logger.info('reply_text,'+reply_text)
                    print "--------------------------------------------"
                    say_text(tts_thread, reply_text, lang_name)
                    moves_thread.suspend()
                    #time.sleep(1)
                    self.startProcessText(None,None)
                    event_starter.start("teks/tp_ct_c_newworldproperty/HandDown","1")

                self.question=int(showQuestion_name);
                print "-> set to question: "+str(self.question)
                event_starter.start("teks/tp_ct_c_newworldproperty/ShowQuestion",str(showQuestion_name))
                #time.sleep(5)
                #event_starter.start("teks/tp_ct_c_newworldproperty/ShowQuestionContent",str(showQuestion_name))

            else:
                #event_starter.start("TEKS/FINISH_SHOW_SCREEN","1")
                #if not reply_text:
                    #reply_text = make_reply_natural(reply_text, lang_name)
                    #lang_name = LAST_SPEECH_LANGUAGE
                    #self.startProcessText(None,None)

                if reply_text:
                    led.eyes_fade(CONFIGURE.NAO_LED_COLOR_TTS_PROCESS)

                    #event_starter.start("teks/StartFaceDetect","1")
                    #randomMoveController.runBR()
                    #speech_lang = check_language(reply_text,lang_name)
                    #if lang_name == CONFIGURE.LANG_CT:

                    #self.stopProcessText(None,None)
                    moves_thread.resume()
                    print "--------------------------------------------"
                    logger.info('reply_text,'+reply_text)
                    print "--------------------------------------------"
                    say_text(tts_thread, reply_text, lang_name)
                    moves_thread.suspend()
                    #time.sleep(1)
                    print "recoding audio" + str(recordingAudio)
                    if recordingAudio == True:
                        # recordingAudio = memProxy.insertData("recordingAudio", False)
                        recordingAudio = False
                        print "The recording audio should be false" + str(recordingAudio)
                        self.stopProcessText(None,None)
                        print "calling stop processing text"
                    else:
                        # memProxy.insertData("recordingAudio", False)
                        self.startProcessText(None,None)
                    event_starter.start("teks/tp_ct_c_newworldproperty/HandDown","1")
                else:
                    recordingAudio = False
                    self.startProcessText(None,None)

                    #elif speech_lang == CONFIGURE.LANG_EN:
                    #  say_text(tts_thread, reply_text, speech_lang)
                    #else:
                    #  say_text(tts_thread, reply_text, lang_name)
                #randomMoveController.stand(3.0)

    def html_popCancel(self,lang_name,value):
        print ">>>>> html_popCancel <<<<<"
        listenController.processText("teks/tp_ct_c_newworldproperty/html","零零零")
        listenController.processText("teks/tp_ct_c_newworldproperty/html","zero zero zero")

    def html_video(self,lang_name,value):
        print ">>>>> html_video <<<<< : "+str(value)
        if str(value)=="play":
            self.stopProcessText(None,None)
        elif str(value)=="stop":
            self.startProcessText(None,None)

    def tts_say(self,name,value):
        global tts
        print "tts say text", value
	tts.say(value);	

    def stop_recording_audio(self,name,value):
        global recordingAudio
        recordingAudio = False
        # memProxy.insertData("recordingAudio", False)
        print "stopped recording audio"
        self.startProcessText(None,None)

    def QuestionHTML_ready(self,name,value):
        print ">>>>> QuestionHTML_ready: "+str(self.question)
        time.sleep(1)
        event_starter.start("teks/tp_ct_c_newworldproperty/ShowQuestionContent",str(self.question))

    def processAnswer(self,name,value):
        print ">>>>> select answer for Q: "+str(self.question)+" <<<<< A:"+str(value)

        if str(value)=="b1b1gotob1b1b1":
            if self.customer_lang==CONFIGURE.LANG_CT:
                listenController.processText("teks/tp_ct_c_newworldproperty/html","流動應用程式a1")
            else:
                listenController.processText("teks/tp_ct_c_newworldproperty/html","app a1")
        elif str(value)=="b1b1gotob1b1b6":
            print "流動應用程式a2"
            if self.customer_lang==CONFIGURE.LANG_CT:
                listenController.processText("teks/tp_ct_c_newworldproperty/html","流動應用程式a2")
                print "流動應用程式a2"
            else:
                listenController.processText("teks/tp_ct_c_newworldproperty/html","app a2")
                print "app a2"
        elif str(value)=="b1b1gotob1b1sl2s1":
            if self.customer_lang==CONFIGURE.LANG_CT:
                listenController.processText("teks/tp_ct_c_newworldproperty/html","最新活動")
            else:
                listenController.processText("teks/tp_ct_c_newworldproperty/html","app a2")
        elif str(value)=="b1b1gotob1b1sl3s1":
            if self.customer_lang==CONFIGURE.LANG_CT:
                listenController.processText("teks/tp_ct_c_newworldproperty/html","今日通告")
            else:
                listenController.processText("teks/tp_ct_c_newworldproperty/html","app a2")
        elif str(value)=="b1b1gotob1b1sl4s1":
            if self.customer_lang==CONFIGURE.LANG_CT:
                listenController.processText("teks/tp_ct_c_newworldproperty/html","有咩筍野")
            else:
                listenController.processText("teks/tp_ct_c_newworldproperty/html","app a2")
        elif str(value)=="b1b1gotob1b1sl5s1":
            if self.customer_lang==CONFIGURE.LANG_CT:
                listenController.processText("teks/tp_ct_c_newworldproperty/html","交昜平台")
            else:
                listenController.processText("teks/tp_ct_c_newworldproperty/html","app a2")
        #elif str(value)=="b1b1gotob1b1b3b1":
        #    listenController.processText("teks/tp_ct_c_newworldproperty/html","問題c1")
        #elif str(value)=="b1b1b3b1gotob1b1b3b1b1":
        #    listenController.processText("teks/tp_ct_c_newworldproperty/html","問題c2")
        pass

    def detectInProcess(self,name,value):
        self.detectout_count = -1    #reset detectout_counter

        #print ">>>>>detectIn: START of detect In <<<<<, self.movement_now="+str(self.movement_now)+", self.detect_in="+str(self.detect_in)

        if self.movement_now==False:
            if self.detect_in == False:
                self.detect_in = True
                print ">>>>>detectIn<<<<<"
                #event_starter.start("teks/tp_ct_c_newworldproperty/html_index1","1")
                # if self.numofface > 1:
                #     event_starter.start("teks/tp_ct_c_newworldproperty/html","零零二")
                # else:
                #     event_starter.start("teks/tp_ct_c_newworldproperty/html","零零一")
                event_starter.start("teks/tp_ct_c_newworldproperty/html","零零一")
                self.startProcessText(None,None)
        else:
            self.detect_in = True

        #print ">>>>>detectIn: END of detect In <<<<<"

    def detectOutProcess(self,name,value):
        if self.detect_in == False:
            return

        if self.detectout_count >=0:
            #self.detectout_count = 0    #another detectOutProcess is running, reset counter from 0
            #print ">>>>>detectOut: another detectOutProcess is running"
            return
        else:
            self.detectout_count=0
            #print ">>>>>detectOut: count: "+str(self.detectout_count)

            while True:
                time.sleep(1)

                if self.detectout_count==-1:    #detect in reset
                    #print ">>>>>detectOut: count reset"
                    return

                self.detectout_count+=1
                print ">>>>>detectOutProcess count: "+str(self.detectout_count)

                if self.detectout_count>=5:
                    self.detectout_count=-1     #stop counter
                    #print ">>>>>detectOut: START of detect Out <<<<<, self.movement_now="+str(self.movement_now)+", self.detect_in="+str(self.detect_in)

                    if self.movement_now==False:
                        self.detect_in = False
                        print ">>>>>detectOut<<<<<"
                        self.stopProcessText(None,None)

                        #event_starter.start("teks/tp_ct_c_newworldproperty/FaceDetectOff","1")
                        trackerProxy.stopTracker()
                        motionProxy.setStiffnesses("Head", 1.0)
                        motionProxy.setAngles("HeadYaw",0,0.2)

                        #event_starter.start("teks/tp_ct_c_newworldproperty/html_index","1")
                        event_starter.start("teks/tp_ct_c_newworldproperty/CallFunction","cancelSpecPopUp|'raiseEvent'")
                        event_starter.start("teks/tp_ct_c_newworldproperty/CallFunction","goToSpecPos|'b1'")
                        event_starter.start("teks/tp_ct_c_newworldproperty/Stand","1")

                        #---start again---
                        trackerProxy.setEffector("None")
                        trackerProxy.setMode("Head")
                        trackerProxy.track("Face")
                    else:
                        event_starter.start("teks/tp_ct_c_newworldproperty/html_reset","1")

                    #print ">>>>> END of detect Out <<<<<"
                    return

    def detectNumFace(self,name,value):
        #print "~~~~~~~~~~~~~~~~~~~~~~~~ detect "+str(value)+" face ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
        self.numofface = int(value)-1
        pass

    def detectNoFace(self,name,value):
        #print "~~~~~~~~~~~~~~~~~~~~~~~~ detect NO face ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
        self.numofface = 0
        pass

    def movementStartProcess(self,name,value):
        self.movement_now = True
        self.stopProcessText(None,None)
        print ">>>>> movement start, stop process text <<<<<"
        #event_starter.start("teks/tp_ct_c_newworldproperty/FaceDetectOff","1")
        trackerProxy.stopTracker()
        motionProxy.setStiffnesses("Head", 1.0)
        motionProxy.setAngles("HeadYaw",0,0.2)

    def movementEndProcess(self,name,value):
        self.detectInProcess(None,None) #SC want to wait for the reset
        self.movement_now = False

        if self.detect_in==True:
            self.startProcessText(None,None)
            event_starter.start("teks/tp_ct_c_newworldproperty/Stand","1")
            print ">>>>> movement end, detect_in=1, start_process_text <<<<<"
        else:
            self.stopProcessText(None,None)
            #event_starter.start("teks/tp_ct_c_newworldproperty/html_index","1")
            event_starter.start("teks/tp_ct_c_newworldproperty/CallFunction","cancelSpecPopUp|'raiseEvent'")
            event_starter.start("teks/tp_ct_c_newworldproperty/CallFunction","goToSpecPos|'b1'")
            event_starter.start("teks/tp_ct_c_newworldproperty/Stand","1")
            print ">>>>> movement end, detect_in=0, stand <<<<<"
        trackerProxy.setEffector("None")
        trackerProxy.setMode("Head")
        trackerProxy.track("Face")

    #
    # def qr_start(self,name,value):
    #     global qr_running
    #     if qr_running == False:
    #         qr_running = True
    #         self.stopProcessText(None,None)
    #
    #         #---stop tracker and move reset head position
    #         global trackerProxy
    #         trackerProxy.stopTracker()
    #         motionProxy.setStiffnesses("Head", 1.0)
    #         motionProxy.setAngles("HeadYaw",0,0.2)
    #         motionProxy.setAngles("HeadPitch",-0.9,0.2)
    #
    #         #---start QR scan
    #         print ">>1>>> processText - qr_start"
    #         os.system("export LD_LIBRARY_PATH=/home/nao/.local/lib:$LD_LIBRARY_PATH; export PYTHONPATH=/home/nao/.local/lib/python2.7/site-packages:$PYTHONPATH; python /home/nao/.local/share/PackageManager/apps/citibank_pepper/script/main.py")
    #     else:
    #         print ">>>1>>>>> qr scan already started"
    #
    # def qr_quit(self,name,value):
    #     global qr_running
    #     print "qr_quit"
    #     #self.logger.verbose('Attempting to start tablet webview')
    #     #tablet = self.s.ALTabletService
    #     #tablet.resetTablet()
    #     #if tablet:
    #     #robot_ip = tablet.robotIp()
    #
    #     if self.detect_in and self.movement_now==False:
    #         print "-> qr_timeout, show_tablet_original, index1.html"
    #         self.startProcessText(None,None)
    #         #app_url = 'http://{}/apps/{}/'.format(robot_ip, "pricerite_pepper")+'index1.html'
    #         event_starter.start("teks/html_index1","1")
    #     else:
    #         print "-> qr_timeout, show_tablet_original, index.html"
    #         #app_url = 'http://{}/apps/{}/'.format(robot_ip, "pricerite_pepper")+'index.html'
    #         event_starter.start("teks/html_index","1")
    #     #tablet.showWebview(app_url)
    #     #else:
    #     #    self.logger.warning('Lost tablet service, cannot load ' +
    #     #                        'application: {}'.format(self.PKG_ID))
    #
    #     #---start head tracker again
    #     trackerProxy.setEffector("None")
    #     trackerProxy.setMode("Head")
    #     trackerProxy.track("Face")
    #
    #     qr_running = False
    #
    # def qr_succ(self,name,value):
    #     global qr_running
    #     print "qr_succ"
    #     #self.logger.verbose('Attempting to start tablet webview')
    #     #tablet = self.s.ALTabletService
    #     #tablet.resetTablet()
    #     #if tablet:
    #     #robot_ip = tablet.robotIp()
    #
    #     #say_text(tts_thread, "親愛的實惠會員，歡迎蒞臨實惠旺角旗艦店，你已成功領取會員優惠，感謝閣下對實惠的踴躍支持！", "Cantonese")
    #     say_text(tts_thread, "親愛的會員，你已成功領取會員優惠，感謝閣下的踴躍支持！", "Cantonese")
    #
    #     if self.detect_in:
    #         print "-> qr_succ, show_tablet_succ, index1.html"
    #         self.startProcessText(None,None)
    #         #app_url = 'http://{}/apps/{}/'.format(robot_ip, "pricerite_pepper")+'index1.html'
    #         event_starter.start("teks/html_index1","1")
    #     else:
    #         print "-> qr_succ, show_tablet_succ, index.html"
    #         #app_url = 'http://{}/apps/{}/'.format(robot_ip, "pricerite_pepper")+'index.html'
    #         event_starter.start("teks/html_index","1")
    #     #tablet.showWebview(app_url)
    #
    #     #else:
    #     #    self.logger.warning('Lost tablet service, cannot load ' +
    #     #                        'application: {}'.format(self.PKG_ID))
    #
    #     #---start head tracker again
    #     trackerProxy.setEffector("None")
    #     trackerProxy.setMode("Head")
    #     trackerProxy.track("Face")
    #
    #     qr_running = False
    #
    # def qr_fail(self,name,value):
    #     global qr_running
    #     print "qr_fail"
    #     #self.logger.verbose('Attempting to start tablet webview')
    #     #tablet = self.s.ALTabletService
    #     #tablet.resetTablet()
    #     #if tablet:
    #     #robot_ip = tablet.robotIp()
    #
    #     say_text(tts_thread,"此會員"+"帳號的優惠已被領取" , "Cantonese")
    #
    #     if self.detect_in:
    #         print "-> qr_fail, show_tablet_fail, index1.html"
    #         self.startProcessText(None,None)
    #         #app_url = 'http://{}/apps/{}/'.format(robot_ip, "pricerite_pepper")+'index1.html'
    #         event_starter.start("teks/html_index1","1")
    #     else:
    #         print "-> qr_fail, show_tablet_fail, index.html"
    #         #app_url = 'http://{}/apps/{}/'.format(robot_ip, "pricerite_pepper")+'index.html'
    #         event_starter.start("teks/html_index","1")
    #     #tablet.showWebview(app_url)
    #
    #     #else:
    #     #    self.logger.warning('Lost tablet service, cannot load ' +
    #     #                        'application: {}'.format(self.PKG_ID))
    #
    #     #---start head tracker again
    #     trackerProxy.setEffector("None")
    #     trackerProxy.setMode("Head")
    #     trackerProxy.track("Face")
    #
    #     qr_running = False
    #
    # def qr_notmember(self,name,value):
    #     global qr_running
    #     print "qr_notmember"
    #     #self.logger.verbose('Attempting to start tablet webview')
    #     #tablet = self.s.ALTabletService
    #     #tablet.resetTablet()
    #     #if tablet:
    #     #robot_ip = tablet.robotIp()
    #
    #     say_text(tts_thread,"系統未能找到此會員帳號" , "Cantonese")
    #
    #     if self.detect_in:
    #         print "-> qr_notmember, show_tablet_notmember, index1.html"
    #         self.startProcessText(None,None)
    #         #app_url = 'http://{}/apps/{}/'.format(robot_ip, "pricerite_pepper")+'index1.html'
    #         event_starter.start("teks/html_index1","1")
    #     else:
    #         print "-> qr_notmember, show_tablet_notmember, index.html"
    #         #app_url = 'http://{}/apps/{}/'.format(robot_ip, "pricerite_pepper")+'index.html'
    #         event_starter.start("teks/html_index","1")
    #     #tablet.showWebview(app_url)
    #
    #     #else:
    #     #    self.logger.warning('Lost tablet service, cannot load ' +
    #     #                        'application: {}'.format(self.PKG_ID))
    #
    #     #---start head tracker again
    #     trackerProxy.setEffector("None")
    #     trackerProxy.setMode("Head")
    #     trackerProxy.track("Face")
    #
    #     qr_running = False

    def process_angle(self, angle):
        global trackerProxy

        if self.movement_now == False:
            print("---------------ANGLE: over the limits: "+str(angle)+"---------------")
            #self.logger.info("ANGLE: over the limits: "+str(angle))

            trackerProxy.stopTracker()
            #trackerProxy.unregisterTarget("Face")

            motionProxy.setStiffnesses("Head", 1.0)
            # Simple command for the HeadYaw joint at 10% max speed
            #names            =
            #angles           = 30.0*almath.TO_RAD
            #fractionMaxSpeed = 0.1
            motionProxy.setAngles("HeadYaw",0,0.2)

            #---start again---
            trackerProxy.setEffector("None")
            #trackerProxy.registerTarget("Face", 0.1)
            #trackerProxy.setRelativePosition([-0.3, -1, 0, 0.1, 0.1, 0.3])
            trackerProxy.setMode("Head")

            trackerProxy.track("Face")
            #print("ANGLE: DONE")
        pass

    def stopFaceTracker(self,name,value):
        event_starter.start("teks/tp_ct_c_newworldproperty/FaceDetectOff","1")
        #motionProxy.setStiffnesses("Head", 1.0)
        #motionProxy.setAngles("HeadYaw",0,0.2)

    def startFaceTracker(self,name,value):
        trackerProxy.setEffector("None")
        #trackerProxy.registerTarget("Face", 0.1)
        #trackerProxy.setRelativePosition([-0.3, -1, 0, 0.1, 0.1, 0.3])
        trackerProxy.setMode("Head")

        trackerProxy.track("Face")

    def searchneo4j(self,name, neo4jQuery):
        # print "hello"
        print "hello"
        print "neo4jquery" + str(neo4jQuery)
        print "neo4jquery" + str(neo4jQuery.encode("utf8"))
        print "neo4jquery" + str(neo4jQuery.encode("utf8").strip())
        # print "name" + name

        # question = neo4jQuery.question.encode('utf-8')
        # lang = neo4jQuery.lang
        tts.say_text("我準備好")
        print "question" + str(question)
        print "lang" + str(lang)
        with self.rlock:
            suspend_listening_flag = True
            index = value
            logging.info(str(index) +"=*_*_*_*_*_*_*_*_*_*_*_*_")
            # queryResult = facility_text_list[index].encode('utf-8')
            # logging.info(queryResult + "-----------------")
            # event_starter.start("teks/tp_ct_c_newworldproperty/content_text", queryResult)
            # tts.say_text(question.encode('utf-8'))

    def processHtmlQuery(self, name, value):
        global facility_text_list
        global tts
        global suspend_listening_flag
        # tts.say_text("hello")
        with self.rlock:
            suspend_listening_flag = True
            index = value
            logging.info(str(index) +"=*_*_*_*_*_*_*_*_*_*_*_*_")
            queryResult = facility_list[index].encode('utf-8')
            logging.info(queryResult + "-----------------")
            event_starter.start("teks/tp_ct_c_newworldproperty/content_text", queryResult)
            listenController.processText(CONFIGURE.LANG_CT, queryResult)
            # tts.say_text(queryResult)

    def setRunningApp(self, key, app):
        # if app == "facility":
        #     self.startProcessText(None,None)
        # elif app == "feedback":
        #     self.startProcessText(None,None)
        if app == "main":
            event_starter.start("teks/tp_ct_c_newworldproperty/html_index","")
        # else:
        #     self.startProcessText(None,None)
        self.startProcessText(None,None)
        if app == "feedback":
            # tts.say_text("請問你有關於邊方面嘅意見想提供? 客務處、保安、清潔、會所、停車場、村巴、園藝淨其他呀?")
            print "running app now"

        runningApp = memProxy.insertData("runningApp", app)
        if app!=None:
            print "setRunning app: "+app
        else:
            print "setRunning app: Main"
        print key
        print runningApp

    def closeApp(self):
        global tts
        print "closeing app"
        tts.say_text("对唔住 我要離開")

    '''def dispatchBehavior(self):
        global app_starter
        global loc_name

        if loc_name:
          logging.info("dispatchBehavior: "+loc_name)
          if loc_name == "frontdoor":
              app_starter.start("teks_pepperuno/goto_front_door")
              say_text(tts_thread, "here you go", CONFIGURE.LANG_EN)
          elif loc_name == "gotostanley":
              app_starter.start("teks_pepperuno/goto_stanley")
              say_text(tts_thread, "here you go", CONFIGURE.LANG_EN)

          loc_name = None

        pass'''

################################################################################
ip = "127.0.0.1"
port = 9559
myBroker = ALBroker("myBroker",
    "0.0.0.0",    # listen to anyone
    0,            # find a free port and use it
    ip,          # parent broker IP
    port)        # parent broker port

memProxy = ALProxy("ALMemory", ip, port)
prefMgrProxy = ALProxy("ALPreferenceManager", ip, port)
motionProxy = ALProxy("ALMotion", ip, port)
managerProxy = ALProxy("ALBehaviorManager", ip, port)
trackerProxy = ALProxy("ALTracker", ip, port)
animatedSpeechProxy = ALProxy("ALAnimatedSpeech", ip, port)
tableService = ALProxy("ALTabletService", ip, port)
tts = ALProxy("cantonese_tts_service", ip, port)


#print tableService.robotIp()
#print tableService.getWifiStatus()
#print "====================================================="

qr_running = False

session = qi.Session()
try:
    session.connect("tcp://" + ip + ":" + str(port))
except RuntimeError:
    logging.info("Can't connect to Naoqi at ip \"" + ip + "\" on port " + str(port) +".\nPlease check your script arguments. Run with -h option for help.")
    sys.exit(1)

listenController = ListeningControlModule("listenController")
listenController.setLock(rlock)
listenController.onLoad()

if __name__ == '__main__':
  from teks_db import teks_db_api

  app_starter = app_starter_module("app_starter")
  event_starter = event_starter_module("event_starter")

  #db_log = teks_db_api.db_log_module("./teks_db/ui_counters.db")
  #db_log.clear_counter("ui_counters","counter")
  #db_log.close()

  write_pid_file("pid.log")

  randomMoveController = RandomMoveControlModule("randomMoveController")

  # motionProxy.wakeUp()

  #memProxy.subscribeToEvent("teks/SaySeen","listenController", "SaySeen")
  #memProxy.subscribeToEvent("teks/SayMaybeSeen","listenController", "SayMaybeSeen")

  #memProxy.subscribeToEvent("teks/FoundFace","listenController", "foundFace")
  #memProxy.subscribeToEvent("teks/NotFoundFace","listenController", "notFoundFace")
  memProxy.subscribeToEvent("FaceDetected","listenController", "faceDetected")
  #memProxy.subscribeToEvent("teks/NotFaceDetected","listenController", "stopFace")

  #memProxy.subscribeToEvent("teks/StopProcessText","listenController", "stopProcessText")
  #memProxy.subscribeToEvent("teks/StartProcessText","listenController", "startProcessText")

  memProxy.subscribeToEvent("teks/tp_ct_c_newworldproperty/DetectNumFace","listenController", "detectNumFace")
  memProxy.subscribeToEvent("teks/tp_ct_c_newworldproperty/DetectNoFace","listenController", "detectNoFace")
  memProxy.subscribeToEvent("teks/tp_ct_c_newworldproperty/DetectIn","listenController", "detectInProcess")
  memProxy.subscribeToEvent("teks/tp_ct_c_newworldproperty/DetectOut","listenController", "detectOutProcess")

  memProxy.subscribeToEvent("teks/tp_ct_c_newworldproperty/MovementStart","listenController", "movementStartProcess")
  memProxy.subscribeToEvent("teks/tp_ct_c_newworldproperty/MovementEnd","listenController", "movementEndProcess")

  memProxy.subscribeToEvent("teks/tp_ct_c_newworldproperty/html","listenController", "processText")
  memProxy.subscribeToEvent("teks/tp_ct_c_newworldproperty/html_popCancel","listenController", "html_popCancel")
  memProxy.subscribeToEvent("teks/tp_ct_c_newworldproperty/html_video","listenController", "html_video")

  memProxy.subscribeToEvent("teks/tp_ct_c_newworldproperty/html_touch","listenController", "processAnswer")
  memProxy.subscribeToEvent("teks/tp_ct_c_newworldproperty/QuestionHTML_ready","listenController", "QuestionHTML_ready")
  memProxy.subscribeToEvent("teks/tp_ct_c_newworldproperty/setrunningapp","listenController", "setRunningApp")
  memProxy.subscribeToEvent("teks/tp_ct_c_newworldproperty/closesubapps","listenController", "closeapp")
  memProxy.subscribeToEvent("teks/tp_ct_c_newworldproperty/index", "listenController", "processHtmlQuery")
  memProxy.subscribeToEvent("teks/tp_ct_c_newworldproperty/searchneo4j", "listenController", "searchneo4j")
  memProxy.subscribeToEvent("teks/tp_ct_c_newworldproperty_feedback/stoppedrecording", "listenController","stop_recording_audio")
  memProxy.subscribeToEvent("teks/tp_ct_c_newworldproperty/ttssay", "listenController","tts_say")

  # memProxy.subscribeToEvent("teks/tp_ct_c_newworldproperty_feedback/startedfacility", "listenController","startedfacility")
  # memProxy.subscribeToEvent("teks/tp_ct_c_newworldproperty_feedback/stoppedrecording", "listenController","stop_recording_audio")
  # memProxy.subscribeToEvent("teks/tp_ct_c_newworldproperty_feedback/recordstopped","listenController","processText")
  memProxy.insertData("runningApp", "Main")
  #memProxy.subscribeToEvent("teks/html_qrscan","listenController", "qr_start")
  #memProxy.subscribeToEvent("teks/qr_quit","listenController", "qr_quit")
  #memProxy.subscribeToEvent("teks/qr_succ","listenController", "qr_succ")
  #memProxy.subscribeToEvent("teks/qr_fail","listenController", "qr_fail")
  #memProxy.subscribeToEvent("teks/qr_notmember","listenController", "qr_notmember")



  import os

  import pyaudio
  p1 = pyaudio.PyAudio()
  devcount = p1.get_device_count()

  logging.info("Here are the available audio devices:")

  default_device_index = 0

  for device_index in range(devcount):
      device = p1.get_device_info_by_index(device_index)
      logging.info("[%s]  %s\tDefault Sample Rate: %i\t%s" % (device_index, device['name'], device['defaultSampleRate'], "Default" if device_index == default_device_index else ""))
      if device['name']=="pulse":
        print ">>> current device_index: "+str(device_index)
        record_thread = teks_record_thread(pill2kill, rlock,"yue-Hant-HK",device_index)

  tts_thread = teks_tts_thread(pill2kill, rlock)
  #record_thread = teks_record_thread(pill2kill, rlock,"yue-Hant-HK",7)
  moves_thread = teks_speech_moves_thread(pill2kill, rlock)

  #record_thread = teks_record_thread(pill2kill, rlock,"yue-Hant-HK",1)
  #record_thread.record_device_index = 7
  #record_thread.set_input(CURRENT_LANGUAGE)

  watch_dog_thread = teks_watch_dog_thread(pill2kill, rlock)
  watch_dog_headyaw_thread = teks_watch_dog_dcm_thread(pill2kill, rlock, memProxy, listenController.process_angle, "Device/SubDeviceList/HeadYaw/Position/Actuator/Value", -math.pi/4, math.pi/4)

  omba_proc_ct = omba_processor("./pepper/ct/brain")
  omba_proc_cn = None
  omba_proc_en = omba_processor("./pepper/en/brain")
  #omba_proc_en = None


  tts_thread.start()
  record_thread.start()
  moves_thread.start()

  watch_dog_thread.start()
  watch_dog_thread.set_name("I/am/watch/dog")

  watch_dog_headyaw_thread.start()
  watch_dog_headyaw_thread.set_name("I/am/watch/dog/headyaw")

  led = led_state()
  aplayer = audio_player()

  app_starter_chatbot = app_starter_module("app_starter_chatbot")
  app_starter_chatbot.start_aysn("tp_ct_c_newworldproperty/main")

  #app_starter_facerecog = app_starter_module("app_starter_facerecog")
  #app_starter_facerecog.start_aysn("teks_face_recog/main")

  #event_starter.start("teks/StartFaceDetect","1")
  #face_recog_once = False

  curPath = os.path.dirname(os.path.abspath(__file__))

  uid = CONFIGURE.REQUEST_UID
  url = CONFIGURE.REQUEST_URL

  LAST_SPEECH_LANGUAGE = CONFIGURE.LANG_CT
  prefMgrProxy.setValue("robot.config.userlang", "spokenlanguage", LAST_SPEECH_LANGUAGE)

  #CONFIGURE.SPEECH_GOOGLE_LANGS = [CONFIGURE.GOOG_LANG_CT, CONFIGURE.GOOG_LANG_CN, CONFIGURE.GOOG_LANG_EN]
  #CONFIGURE.SPEECH_RECOG_LANGS = {CONFIGURE.GOOG_LANG_CT:CONFIGURE.LANG_CT, CONFIGURE.GOOG_LANG_CN:CONFIGURE.LANG_CN, CONFIGURE.GOOG_LANG_EN:CONFIGURE.LANG_EN}

  #CONFIGURE.SPEECH_GOOGLE_LANGS = [CONFIGURE.GOOG_LANG_CT, CONFIGURE.GOOG_LANG_CN]
  #CONFIGURE.SPEECH_RECOG_LANGS = {CONFIGURE.GOOG_LANG_CT:CONFIGURE.LANG_CT, CONFIGURE.GOOG_LANG_CN:CONFIGURE.LANG_CN}

  CONFIGURE.SPEECH_GOOGLE_LANGS = [CONFIGURE.GOOG_LANG_CT]

  CONFIGURE.SPEECH_RECOG_LANGS = {CONFIGURE.GOOG_LANG_CT:CONFIGURE.LANG_CT}

  #CONFIGURE.SPEECH_GOOGLE_LANGS = [CONFIGURE.GOOG_LANG_CT]
  #CONFIGURE.SPEECH_RECOG_LANGS = {CONFIGURE.GOOG_LANG_CT:CONFIGURE.LANG_CT}

  print '\n*** INITIALIZE SUCCESS ***\n'

  facility_list = ["乒乓波","桌球", "游水", "健身", "浴室","桑拿室", "蒸氣室", "保齡球", "燒野食","鋼琴室","瑜伽", "宴會廳一","宴會廳二","宴會廳三","宴會廳五","乒乓波, 桌球, 游水, 健身, 保齡球, 燒野食, 幼兒探索天地, 你對哶設施有興趣?"]
  feedBackList = ["客務處","保安","清潔","會所","停車場","村巴","園藝","其他"]
  feedBackIntroduction = "請問你有關於邊方面嘅意見想提供? 客務處、保安、清潔、會所、停車場、村巴、園藝淨其他呀?"
  facility_text_list = []
  graph = neo4j_graph.authorize_dialog()
  print "facility list " + str(facility_list)
  try:
    for item in facility_list:
        content = db_tools.search_answer(graph, item, logger, language="CT")
        logging.info(item + ": " + content)
        facility_text_list.append(content)
  except Exception as e:
        print e
        facility_text_list = []

  import random
  '''
  wait_text_list = [
                        "",
                        "請等等",
                        "",
                        "請等一等",
                        "",
                        "唔該等等",
                        "",
                        "唔該等一等",
                        "",
                        "好"]
  '''
  '''wait_text_list = [
                        "",
                        "",
                        "唔",
                        "",
                        "",
                        "好"]'''

  user_best = {"rank": [3, "C"], "clear-time":55, "clear-count":5}



  try:
    #record_thread.resume()
    suspend_listening_flag = False
    while True:
      try:
        #check to proceed or not
        if suspend_listening_flag:
            time.sleep(0.1)
            continue

        #p1 = pyaudio.PyAudio() #alan_ykl new
        #print "CURRENT_LANGUAGE:", CURRENT_LANGUAGE
        # STT process
        '''
        with rlock:
          led.eyes_fade(CONFIGURE.NAO_LED_COLOR_ASR_PROCESS)
          aplayer.play(curPath+"/begin_reco.wav")
        '''
        start_time = time.time()
        input_text, speech_lang = listen_text(record_thread, CURRENT_LANGUAGE)

        '''
        wait_text = random.choice(wait_text_list)
        if wait_text and input_text:
          say_text(tts_thread, wait_text, CURRENT_LANGUAGE)
        '''
        '''
        with rlock:
          led.eyes_fade(CONFIGURE.NAO_LED_COLOR_PROCESS_PROCESS)
          aplayer.play(curPath+"/end_reco.wav")
        '''
        '''
        lang_setting = check_current_language(input_text)
        if lang_setting:
          CURRENT_LANGUAGE = set_current_language(tts_thread, lang_setting, CURRENT_LANGUAGE)
          continue
        '''

        if input_text:
            logging.info("============Listen===========(duration:"+str(time.time()-start_time)+")======")
            input_text = input_text.encode("utf8")
            logging.info("["+speech_lang+"] "+input_text)
            logging.info("=========================================================")

            #--store to database
            #db_log = teks_db_api.db_log_module("./teks_db/ui_counters.db")
            #db_log.inc_counter_by_key("ui_counters",input_text,"counter")
            #db_log.close()

            #msg = raw_input('You> combined_row,'
            #msg = c1.lower()

            #--check for exit program
            #logger.info('input_text_5,'+input_text+' '+speech_lang)
            if ("永远再见" == input_text) or ("永再見" == input_text) or ("永遠再見" == input_text) or ("forever bye bye" == input_text.lower()):
                event_starter.start("TEKS/StandUpPose","1")
                break

            #if speech_lang != CONFIGURE.LANG_CT:
            #  CURRENT_LANGUAGE = CONFIGURE.LANG_CT

            #  logging.info("not really Cantonese")
            #  reply_text = "我讀得書少 淨係識廣東話 你唔好恰我"
            #  moves_thread.resume()
            #  say_text(tts_thread, reply_text, CURRENT_LANGUAGE)
            #  moves_thread.suspend()

            #  LAST_SPEECH_LANGUAGE = CURRENT_LANGUAGE
            #reset back to use Cantonese ASR to listen English and Cantonese
            #  with rlock:
            #    time.sleep(1.0)
            #    suspend_listening_flag = False
            #  continue

            #--process to check
            if listenController.detect_in==True:
                print "detect is true"
                listenController.processText(speech_lang,input_text)

            last_recognized_face = None

        if watch_dog_thread.is_modified():
            #--reset flag here
            logging.info("======RiveScript updated=========================================")
            omba_proc_ct = omba_processor("./pepper/ct/brain")
            #omba_proc_cn = omba_processor("./pepper/cn/brain")
            omba_proc_en = omba_processor("./pepper/en/brain")

            #reply = omba_proc_ct.process_text("一零一")

            watch_dog_thread.reset_modified_flag()

        LAST_SPEECH_LANGUAGE = CURRENT_LANGUAGE

        #reset back to use Cantonese ASR to listen English and Cantonese
        CURRENT_LANGUAGE = CONFIGURE.LANG_CT

        with rlock:
          #time.sleep(1.0)
          suspend_listening_flag = False

        '''
        input_json = {"inputText": input_text, "uid": uid};

        start_time = time.time()
        req = requests.post(url, json = input_json)
        if req:
          print "post time: " + str(time.time() - start_time)

          print '\n*** REQUEST RETURNS ***\n'
          req_content = str(req.content)
          print "req_content: " + req_content

          reply_text = get_reply_from_json(req_content)
          print "reply_text: " + reply_text

          reply_text = make_reply_natural(reply_text, CURRENT_LANGUAGE)
          print "我 > " + reply_text
          led.eyes_fade(CONFIGURE.NAO_LED_COLOR_TTS_PROCESS)
          say_text(tts_thread, reply_text, CURRENT_LANGUAGE)
        '''

      except Exception as e:
        print e
        pill2kill.set()
        pass

  except Exception as e:
    print e

  finally:
    print '\n*** WAIT FOR CHILD THREADS EXIT ***\n'

    #save_unmatched_sentence(UNMATCHED_SENTENCE_LIST)  # If rivescript can match nothing, save the text to file.
    app_starter_chatbot.stop("tp_ct_c_newworldproperty/main")
    #app_starter_facerecog.stop("teks_face_recog/main")

    pill2kill.set()

    tts_thread.join()
    record_thread.join()
    moves_thread.join()
    watch_dog_thread.join()
    watch_dog_headyaw_thread.join()

    led.eyes_reset()

    print '\n*** STANDING UP ***\n'
    event_starter.start("teks/ManuallyEndPythonApp","1")
    randomMoveController.stand(3.0)

    print '\n*** FINISH ***\n'
