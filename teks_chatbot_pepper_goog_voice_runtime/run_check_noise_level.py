#!/usr/bin/env python
#  -*- coding:utf-8 -*-

import sys
import time
import json
import requests
import threading
import teks_configure as CONFIGURE
from teks_utility import *
#from teks_asr_ifly import teks_record_thread
from teks_asr_goog_beta import teks_record_thread
from teks_tts import teks_tts_thread
from teks_led_state import led_state
from teks_audio_player import audio_player

from teks_omba_utils import omba_processor

import logging
import teks_logger as teks_log

DEFAULT_LOGGER_FORMATTER = logging.Formatter('%(message)s,%(levelname)s,"%(asctime)s"')
DEFAULT_LOGGER_LEVEL = logging.INFO

logger = teks_log.teks_logger("teks_demo", \
                     DEFAULT_LOGGER_FORMATTER, \
                     None, \
                     DEFAULT_LOGGER_LEVEL)


################################################################################

UNMATCHED_SENTENCE_LIST = []
CURRENT_LANGUAGE = CONFIGURE.LANG_CT
#CURRENT_LANGUAGE = CONFIGURE.LANG_CN

pill2kill = threading.Event()
rlock = threading.RLock()

################################################################################

if __name__ == '__main__':
  import os
  pid = str(os.getpid())
  print pid
  f = open('./pid.log', 'w')
  f.write(pid)
  f.close()

  from teks_db import teks_db_api

  #db_log = teks_db_api.db_log_module("./teks_db/ui_counters.db")
  #db_log.clear_counter("ui_counters","counter")
  #db_log.close()

  randomMoveController = RandomMoveControlModule("randomMoveController")

  import os

  import pyaudio
  p1 = pyaudio.PyAudio()
  devcount = p1.get_device_count()

  logging.info("Here are the available audio devices:")

  default_device_index = 0

  for device_index in range(devcount):
      device = p1.get_device_info_by_index(device_index)
      logging.info("[%s]  %s\tDefault Sample Rate: %i\t%s" % (device_index, device['name'], device['defaultSampleRate'], "Default" if device_index == default_device_index else ""))
      if device['name']=="pulse":
        print ">>> current device_index: "+str(device_index)
        record_thread = teks_record_thread(pill2kill, rlock,"yue-Hant-HK",device_index)

  tts_thread = teks_tts_thread(pill2kill, rlock)
  #record_thread = teks_record_thread(pill2kill, rlock,"yue-Hant-HK",7)
  #record_thread = teks_record_thread(pill2kill, rlock,"yue-Hant-HK",1)
  #record_thread.record_device_index = 7
  #record_thread.set_input(CURRENT_LANGUAGE)

  omba_proc_ct = omba_processor("./pepper/ct/brain")
  omba_proc_cn = omba_processor("./pepper/cn/brain")
  #omba_proc_ct = omba_processor("./nao/ct/brain")
  #omba_proc_cn = omba_processor("./nao/cn/brain")

  tts_thread.start()
  record_thread.start()

  led = led_state()
  aplayer = audio_player()

  app_starter_chatbot = app_starter_module("app_starter_chatbot")
  app_starter_chatbot.start_aysn("pepper_chatbot_demo/behavior_1")

  app_starter = app_starter_module("app_starter")
  event_starter = event_starter_module("event_starter")

  curPath = os.path.dirname(os.path.abspath(__file__))    

  uid = CONFIGURE.REQUEST_UID
  url = CONFIGURE.REQUEST_URL

  LAST_SPEECH_LANGUAGE = CONFIGURE.LANG_CT

  print '\n*** INITIALIZE SUCCESS ***\n'

  import random
  '''
  wait_text_list = [
                        "",
                        "請等等",
                        "",
                        "請等一等",
                        "",
                        "唔該等等",
                        "",
                        "唔該等一等",
                        "",
                        "好"]
  '''
  wait_text_list = [
                        "",
                        "唔",
                        "",
                        "好"]
  try:
    suspend_listening_flag = False
    while True:
      try:
        #check to proceed or not
        if suspend_listening_flag:
            time.sleep(0.1)
            continue

        print "CURRENT_LANGUAGE:", CURRENT_LANGUAGE
        # STT process
        '''
        with rlock:
          led.eyes_fade(CONFIGURE.NAO_LED_COLOR_ASR_PROCESS)
          aplayer.play(curPath+"/begin_reco.wav")
        '''
        start_time = time.time()
        input_text, speech_lang = listen_text(record_thread, CURRENT_LANGUAGE)
        logging.info("=====================THANK YOU========"+str(time.time()-start_time)+"======")
        '''
        wait_text = random.choice(wait_text_list)
        if wait_text and input_text:
          say_text(tts_thread, wait_text, CURRENT_LANGUAGE)
        '''
        '''
        with rlock:
          led.eyes_fade(CONFIGURE.NAO_LED_COLOR_PROCESS_PROCESS)
          aplayer.play(curPath+"/end_reco.wav")
        '''
        '''
        lang_setting = check_current_language(input_text)
        if lang_setting:
          CURRENT_LANGUAGE = set_current_language(tts_thread, lang_setting, CURRENT_LANGUAGE)
          continue
        '''

        if input_text:
          input_text = c1 = input_text.encode("utf8")
          logging.info( "I heard: "+c1)

          #db_log = teks_db_api.db_log_module("./teks_db/ui_counters.db")
          #db_log.inc_counter_by_key("ui_counters",input_text,"counter")
          #db_log.close()

          #msg = raw_input('You> combined_row,'
          msg = c1.lower()

          logger.info('input_text,'+input_text+' '+speech_lang)
          if ("永远再见" == input_text) or ("永遠再見" == input_text) or ("forever bye bye" == input_text.lower()):
            break

          # check speaking language
          reply = ""
          cur_omba = None
          if speech_lang == CONFIGURE.LANG_CT:
            CURRENT_LANGUAGE = check_language(c1,CURRENT_LANGUAGE)
            cur_omba = omba_proc_ct
            print "check user speaking "+speech_lang
          else:
            CURRENT_LANGUAGE = CONFIGURE.LANG_CN
            cur_omba = omba_proc_cn
            print "check user speaking "+speech_lang

          start_time = time.time()
          reply = cur_omba.process_text(msg)
          logging.info(time.time()-start_time)

          #
          logging.info("=====================My Reply========"+reply.encode("utf8")+"======")
          reply_text = reply.encode("utf8").strip()

          #if reply_text: event_starter.start("Dialog/LastInput","1")

          if cur_omba:
              page_name = cur_omba.bot.get_uservar(CONFIGURE.REQUEST_UID, "pageName")
              beh_name = cur_omba.bot.get_uservar(CONFIGURE.REQUEST_UID, "behName")
              if beh_name and beh_name != "undefined":
                  cur_omba.bot.set_uservar(CONFIGURE.REQUEST_UID, "behName","")
                  logger.info('behName,'+beh_name)
                  logging.info("behName: "+beh_name)
                  if reply_text:
                      led.eyes_fade(CONFIGURE.NAO_LED_COLOR_TTS_PROCESS)
                      say_text(tts_thread, reply_text, CURRENT_LANGUAGE)
                  logger.info('reply_text,'+reply_text)
                  time.sleep(1.0)
                  app_starter.start(str(beh_name))
              else:
                evt_name = cur_omba.bot.get_uservar(CONFIGURE.REQUEST_UID, "evtName")
                if evt_name and evt_name != "undefined":
                    cur_omba.bot.set_uservar(CONFIGURE.REQUEST_UID, "evtName","")
                    evt_param = cur_omba.bot.get_uservar(CONFIGURE.REQUEST_UID, "evtParam")
                    logger.info('evtName,'+evt_name+','+evt_param)
                    logging.info("evtName: "+evt_name)
                    logging.info("evtParam: "+evt_param)
                    event_starter.start(evt_name,str(evt_param))
                    logger.info('reply_text,'+reply_text)
                    if reply_text: 
                        led.eyes_fade(CONFIGURE.NAO_LED_COLOR_TTS_PROCESS)
                        say_text(tts_thread, reply_text, CURRENT_LANGUAGE)
                    if CURRENT_LANGUAGE == CONFIGURE.LANG_CT:
                      event_starter.start("TEKS/StopAnimCantonese","1")
                else:
                    #event_starter.start("TEKS/FINISH_SHOW_SCREEN","1")
                    if not reply_text:
                        #reply_text = make_reply_natural(reply_text, CURRENT_LANGUAGE)
                        CURRENT_LANGUAGE = LAST_SPEECH_LANGUAGE

                    logger.info('reply_text,'+reply_text)

                    led.eyes_fade(CONFIGURE.NAO_LED_COLOR_TTS_PROCESS)

                    if reply_text:
                      if CURRENT_LANGUAGE == CONFIGURE.LANG_CT:
                        event_starter.start("TEKS/SayAnimCantonese","1")
                        #randomMoveController.runBR()
                        #speech_lang = check_language(reply_text,CURRENT_LANGUAGE)
                        '''
                        if CURRENT_LANGUAGE == CONFIGURE.LANG_CT and speech_lang == CONFIGURE.LANG_EN:
                          say_text(tts_thread, reply_text, speech_lang)
                        else:
                          say_text(tts_thread, reply_text, CURRENT_LANGUAGE)
                        '''
                      say_text(tts_thread, reply_text, CURRENT_LANGUAGE)
                    #randomMoveController.stand(3.0)
                    if CURRENT_LANGUAGE == CONFIGURE.LANG_CT:
                      event_starter.start("TEKS/StopAnimCantonese","1")

        LAST_SPEECH_LANGUAGE = CURRENT_LANGUAGE

        #reset back to use Cantonese ASR to listen English and Cantonese
        CURRENT_LANGUAGE = CONFIGURE.LANG_CT
        with rlock:
          time.sleep(1.0)
          suspend_listening_flag = False

        '''
        input_json = {"inputText": input_text, "uid": uid};

        start_time = time.time()
        req = requests.post(url, json = input_json)
        if req:
          print "post time: " + str(time.time() - start_time)

          print '\n*** REQUEST RETURNS ***\n'
          req_content = str(req.content)
          print "req_content: " + req_content

          reply_text = get_reply_from_json(req_content)
          print "reply_text: " + reply_text

          reply_text = make_reply_natural(reply_text, CURRENT_LANGUAGE)
          print "我 > " + reply_text
          led.eyes_fade(CONFIGURE.NAO_LED_COLOR_TTS_PROCESS)
          say_text(tts_thread, reply_text, CURRENT_LANGUAGE)
        '''

      except Exception as e:
        print e
        pill2kill.set()
        pass

  except Exception as e:
    print e

  finally:
    print '\n*** WAIT FOR CHILD THREADS EXIT ***\n'

    #save_unmatched_sentence(UNMATCHED_SENTENCE_LIST)  # If rivescript can match nothing, save the text to file.
    app_starter_chatbot.stop("pepper_chatbot_demo/behavior_1")

    pill2kill.set()

    tts_thread.join()
    record_thread.join()

    led.eyes_reset()

    print '\n*** FINISH ***\n'
