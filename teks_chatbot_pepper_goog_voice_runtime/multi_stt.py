
import threading
import logging
from teks_utility import *

import speech_recognition_teks as sr

class MultiRecognition(object):
    def __init__(self):
        self.result = {'confidence': 0, 'transcript': '', 'lang_code':''}
        self.mylock = threading.RLock()

    def multiThreading(self, recogFunc, audioData, lang_list):
        threads = []
        for i in lang_list:
            threads.append(threading.Thread(target=self.recognition, args=(recogFunc, audioData, i)))

        for t in threads:
            t.start()

        for t in threads:
            t.join()

        self.result_set = self.result
        self.result = {'confidence': 0, 'transcript': '', 'lang_code':''}

        return self.result_set

    def recognition(self, recogFunc, audioData, lang):

        said_text = ""
        confidence = 0
        # adding bias to less favor to cantonese
        ct_bias = 1
        try:
            said_text, confidence = recogFunc(audioData, None, lang)
        except sr.UnknownValueError:
            logging.info("Sorry, not understand in multi stt")
            #self.result = {'confidence': 0, 'transcript': '', 'lang_code':''}

        except sr.RequestError as e:
            logging.info("No connection")
            #self.result = {'confidence': 0, 'transcript': '', 'lang_code':''}

        self.mylock.acquire()
        # set bias over the english
        #
        if lang == "yue-Hant-HK":
            lang_name = check_language(said_text,CONFIGURE.LANG_CT)
            if lang_name!=CONFIGURE.LANG_CT:    #decrease confidence level 0.1 if the STT is pure english
                print "decrease confidence level 0.2 if the STT is pure english"
                if confidence>=0.2:
                    confidence-=0.2
            confidence = confidence*ct_bias
            print "bias_ct:{}".format(confidence)
            
        if self.result['confidence'] < confidence:
            self.result['confidence'] = confidence
            self.result['transcript'] = said_text
            self.result['lang_code'] = lang

        self.mylock.release()
