#!/usr/bin/env python
#  -*- coding:utf-8 -*-
from teks_utility import *

def foulwords_concept_ct(text):
    foulWords = ["贱人", "屌", "屌你", "老母", "屌老母系", "扑街", "食屎", "屎忽", "去死", "操你", "操你妈", "傻逼", "冚家铲", "冚家祥","頂***","*你老母","僕你個街","仆*","你個*","*你個*","廢柴","*你個臭*","臭*","含家產","食屎","撚樣"]
    return match_concept(text, foulWords)

def foulwords_concept_en(text):
    foulWords = ["tmd","piss off", "bullshit", "drop dead", "asshole", "bitch", "son of a bitch", "son of bitch", "shit", "fuck", "fucking", "cun", "your ass"]
    return match_concept(text, foulWords)
